/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.api;

import org.jahia.osgi.FrameworkService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Alexander Storozhuk on 22.06.2015.
 * Time: 16:19
 */
public class DynamicReWriter {

    public static final String OUTBOUND_BASE_ATTR = "DynamicBaseURLRewriteAttr";
    public static final String OUTBOUND_SEO_ATTR = "DynamicSeoURLRewriteAttr";
    public static final String OUTBOUND_LAST_ATTR = "DynamicLastURLRewriteAttr";

    private static String URL_REWRITE_MODULE_NAME = "dynamic-urlrewrite";
    private static final Logger logger = LoggerFactory.getLogger(DynamicReWriter.class);
    private Bundle dynamicRewriteBundle;
    private RewriteCallback rewriteCallback;

    public void doRuleSeo(HttpServletRequest request, HttpServletResponse response, FilterChain parentChain, String url) throws IOException, ServletException {
        doRuleByScope(RewriteCallback.SCOPE_SEO, request, response, parentChain, url);
    }

    public void doRuleLast(HttpServletRequest request, HttpServletResponse response, FilterChain parentChain, String url) throws IOException, ServletException {
        doRuleByScope(RewriteCallback.SCOPE_LAST, request, response, parentChain, url);
    }

    public void doRule(HttpServletRequest request, HttpServletResponse response, FilterChain parentChain, String url) throws IOException, ServletException {
        doRuleByScope(RewriteCallback.SCOPE_BASE, request, response, parentChain, url);
    }

    public void doOutboundRule(HttpServletRequest request, HttpServletResponse response, String url) throws IOException, ServletException, InvocationTargetException {
        String outUrl = doOutboundRuleByScope(RewriteCallback.SCOPE_BASE, request, response, url);
        if (outUrl != null)
            request.setAttribute(OUTBOUND_BASE_ATTR, outUrl);
    }

    public void doOutboundRuleSeo(HttpServletRequest request, HttpServletResponse response, String url) throws IOException, ServletException, InvocationTargetException {
        String outUrl = doOutboundRuleByScope(RewriteCallback.SCOPE_SEO, request, response, url);
        if (outUrl != null)
            request.setAttribute(OUTBOUND_SEO_ATTR, outUrl);
    }

    public void doOutboundRuleLast(HttpServletRequest request, HttpServletResponse response, String url) throws IOException, ServletException, InvocationTargetException {
        String outUrl = doOutboundRuleByScope(RewriteCallback.SCOPE_LAST, request, response, url);
        if (outUrl != null)
            request.setAttribute(OUTBOUND_LAST_ATTR, outUrl);
    }

    private String doOutboundRuleByScope(String scope, HttpServletRequest request, HttpServletResponse response, String url) throws IOException, ServletException, InvocationTargetException {
        return getRewriteCallback() == null ? url : rewriteCallback.rewriteOutbound(scope, url, request, response);
    }

    private void doRuleByScope(String scope, HttpServletRequest request, HttpServletResponse response, FilterChain parentChain, String url) throws IOException, ServletException {
        if (getRewriteCallback() != null)
            getRewriteCallback().processRequest(scope, request, response, parentChain);
    }

    private RewriteCallback getRewriteCallback() {
        if (null == rewriteCallback) {
            if (getDynamicRewriteBundle() == null)
                return null;
            BundleContext bundleContext = dynamicRewriteBundle.getBundleContext();
            ServiceReference realServiceReference = bundleContext.getServiceReference(RewriteCallback.class);
            rewriteCallback = (RewriteCallback) FrameworkService.getInstance().getBundleContext().getService(realServiceReference);
        }
        return rewriteCallback;
    }

    private Bundle getDynamicRewriteBundle() {
        if (null == dynamicRewriteBundle) {
            Bundle[] bundles = FrameworkService.getInstance().getBundleContext().getBundles();
            for (Bundle bundle : bundles) {
                if (bundle.getSymbolicName().equals(URL_REWRITE_MODULE_NAME)) {
                    dynamicRewriteBundle = bundle;
                }
            }
            if (null == dynamicRewriteBundle)
                logger.debug("Can find module: [" + URL_REWRITE_MODULE_NAME + "]");
        }
        return dynamicRewriteBundle;
    }
}
