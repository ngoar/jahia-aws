/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.services;

import org.jahia.modules.external.aws.event.S3Event;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.decorator.JCRMountPointNode;

import javax.jcr.RepositoryException;
import javax.jcr.observation.EventListener;
import java.util.List;
import java.util.Map;

/**
 * Created by Igor Shabanov on 23.06.2015.
 */
public interface AwsService {
    String BEAN_NAME = "awsService";

    List<String> getMountPointsPathList(JCRSessionWrapper session, boolean mounted) throws RepositoryException;

    List<String> getMountPointsPathList(JCRSessionWrapper session) throws RepositoryException;

    List<JCRMountPointNode> getMountpointsNode(JCRSessionWrapper session) throws RepositoryException;

    AwsAccess getAccessConfig(String accessKeyName) throws RepositoryException;

    AwsAccess getAccessConfig(JCRSessionWrapper session, String accessKeyName) throws RepositoryException;

    Map<String, AwsAccess> getAccessConfigs(JCRSessionWrapper session) throws RepositoryException;

    Map<String, AwsAccess> getAccessConfigs(JCRSessionWrapper session, String site) throws RepositoryException;

    void addEventListener(EventListener eventListener);

    void removeEventHandler(EventListener eventListener);

    void addEvent(S3Event event);
}
