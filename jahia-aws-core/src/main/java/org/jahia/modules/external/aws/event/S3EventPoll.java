/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.event;

import org.apache.jackrabbit.rmi.iterator.ArrayEventIterator;
import org.apache.jackrabbit.rmi.observation.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.observation.EventListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Igor Shabanov on 18.06.2015.
 */
public class S3EventPoll extends Thread {

    /** logger */
    private static final Logger log =
            LoggerFactory.getLogger(S3EventPoll.class);

    /**
     * The time in milliseconds the {@link #run()} method should be waiting
     * for remote events.
     * @see #run()
     */
    private static final long POLL_TIMEOUT = 5000;


    private HashSet<EventListener> eventListeners = new HashSet<>();


    private boolean running = true;

    private Queue queue;


    public S3Event[] getNextEvent(long timeout)  {
        // need the queue
        checkQueue();
        try {
            if (timeout < 0) {
                timeout = 0;
            }
            ArrayList<S3Event>result=new ArrayList<>();
            while (true){
                S3Event ob=(S3Event)queue.get(timeout);
                if(null==ob) break;
                result.add(ob);
            }
            if(0==result.size())
                return null;
            S3Event[] reultArr = new S3Event[result.size()];
            for(int i = 0; i<reultArr.length;i++)
                reultArr[i]=result.get(i);
            return reultArr;
        } catch (InterruptedException ie) {
        }

        // did not get anything, fall back to nothing
        return null;
    }

    /**
     * Makes sure, the {@link #queue} field is assigned a value.
     */
    private synchronized void checkQueue() {
        if (queue == null) {
            queue = new Queue();
        }
    }

    /**
     * Returns the <code>Channel</code> allocating it if required.
     *
     * @return queue
     */
    private Queue getQueue() {
        checkQueue();
        return queue;
    }

    public void terminate() {
        this.running = false;
    }

    public void run() {
        try {
            while (true) {
                synchronized (this) {
                    if (!running)
                        break;
                    wait(POLL_TIMEOUT);
                }
                S3Event[] remoteEvents = getNextEvent(POLL_TIMEOUT);
                // poll time out, check running and ask again
                if (remoteEvents == null ) {
                    continue;
                }
                for (EventListener eventListener : eventListeners) {
                    try {
                        eventListener.onEvent(new ArrayEventIterator(remoteEvents));
                    } catch (Exception e) {
                        log.error("Error in S3 Event Handler ",e);
                    } catch (Throwable et) {
                        log.error("Throwable in S3 Event Handler ",et);
                    }

                }
            }
        } catch (InterruptedException ex) {
            log.error("Interrupted S3 Event Poll ",ex);
        } catch (Throwable ex) {
            log.error("Stoped S3 Event Poll ",ex);
        }

    }



    public synchronized  void addEventHandler(EventListener eventListener){
        eventListeners.add(eventListener);
    }
    public synchronized void removeEventHandler(EventListener eventListener){
        eventListeners.remove(eventListener);
    }

    public void addEvent(S3Event event) {
        getQueue().put(event);
    }


}
