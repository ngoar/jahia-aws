/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.rules;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.jackrabbit.spi.commons.conversion.MalformedPathException;
import org.jahia.api.Constants;
import org.jahia.modules.external.aws.providers.AmazonS3DataSource;
import org.jahia.modules.external.aws.providers.S3ExternalStoreProvider;
import org.jahia.services.content.JCRContentUtils;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.render.filter.ContextPlaceholdersReplacer;
import org.jahia.services.visibility.VisibilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

/**
 * Created by Alexander Storozhuk on 03.06.2015.
 * Time: 19:53
 */
public class FileKey {
    private static final Logger logger = LoggerFactory.getLogger(FileKey.class);
    private String cacheKey;

    private String path;
    private String thumbnail;
    private String versionDate;
    private String versionLabel;
    private String workspace;
    private HttpServletResponse hsResponse;


    /**
     * Initializes an instance of this class.
     *
     * @param workspace
     * @param path
     * @param versionDate
     * @param versionLabel
     * @param thumbnail
     */
    private FileKey(String workspace, String path, String versionDate, String versionLabel, String thumbnail, HttpServletResponse hsResponse ) {
        super();
        this.workspace = workspace;
        this.path = path;
        this.versionDate = versionDate;
        this.versionLabel = versionLabel;
        this.thumbnail = thumbnail;
        this.hsResponse = hsResponse;
    }

    public String getCacheKey() {
        if (cacheKey == null) {
            StringBuilder key = new StringBuilder(64);
            key.append(workspace).append(":").append(path).append(":")
                    .append(versionDate == null ? "0" : versionDate).append(":");
            if (versionLabel != null) {
                key.append(versionLabel);
            }
            cacheKey = key.toString();
        }

        return cacheKey;
    }

    public String getPath() {
        return path;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getVersionDate() {
        return versionDate;
    }

    public String getVersionLabel() {
        return versionLabel;
    }

    public String getWorkspace() {
        return workspace;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public HttpServletResponse getHsResponse() {
        return hsResponse;
    }


    public JCRNodeWrapper getNode() throws UnsupportedEncodingException {
        JCRNodeWrapper n = null;
        if (getWorkspace() != null && StringUtils.isNotEmpty(getPath())) {
            JCRSessionWrapper session = null;
            try {
                session = JCRSessionFactory.getInstance().getCurrentUserSession(getWorkspace());

                if (getVersionDate() != null) {
                    session.setVersionDate(new Date(Long.valueOf(getVersionDate())));
                }
                if (getVersionLabel() != null) {
                    session.setVersionLabel(getVersionLabel());
                }
                n = session.getNode(getPath());

                if (!isValidNode(n)) {
                    n = null;
                }
            } catch (RuntimeException e) {
                logger.debug(e.getMessage(), e);
            } catch (PathNotFoundException e) {
                logger.debug(e.getMessage(), e);
            } catch (RepositoryException e) {
                if (e.getCause() != null && e.getCause() instanceof MalformedPathException) {
                    logger.debug(e.getMessage(), e);
                } else {
                    logger.error("Error accesing path: " + getPath() + " for user "
                            + (session != null ? session.getUserID() : null), e.getMessage());
                    n = null;
                    try {
                        if (hsResponse!=null)
                            hsResponse.sendError(404, "Access denied");
                    } catch (Exception ignore) {
                    }
                }
            }
        }
        return n;
    }
    public static FileKey parseKey(String url) throws UnsupportedEncodingException {
        String workspace = null;
        String path = null;
        String filesPrefix = "/files";
        String p = url;
        if (p != null && p.length() > 2 && p.startsWith(filesPrefix)) {
            p = p.substring(filesPrefix.length(), p.length());
            int pathStart = p.indexOf("/", 1);
            workspace = pathStart > 1 ? p.substring(1, pathStart) : null;
            if (workspace != null) {
                path = p.substring(pathStart);
                if (!JCRContentUtils.isValidWorkspace(workspace)) {
                    // unknown workspace
                    workspace = null;
                }
            }
            return path != null && workspace != null ? new FileKey(workspace, JCRContentUtils.escapeNodePath(path),
                    null, null,  StringUtils.EMPTY, null) : null;
        }
        return null;
    }

    public static FileKey parseKey(String url, HttpServletRequest hsRequest, HttpServletResponse hsResponse, String characterEncoding) throws UnsupportedEncodingException {
        String workspace = null;
        String path = null;
        String filesPrefix = "/files";
        String p = hsRequest.getPathInfo() != null ? hsRequest.getPathInfo() : url;
        if (p != null && p.length() > 2) {
            if (p.startsWith(filesPrefix)) {
                p = p.substring(filesPrefix.length(), p.length());
            }
            int pathStart = p.indexOf("/", 1);
            workspace = pathStart > 1 ? p.substring(1, pathStart) : null;
            if (workspace != null) {
                path = p.substring(pathStart);
                if (ContextPlaceholdersReplacer.WORKSPACE_PLACEHOLDER.equals(URLDecoder.decode(
                        workspace, characterEncoding))) {
                    // Hack for CK Editor links
                    workspace = Constants.EDIT_WORKSPACE;
                }
                if (!JCRContentUtils.isValidWorkspace(workspace)) {
                    // unknown workspace
                    workspace = null;
                }
            }
        }

        return path != null && workspace != null ? new FileKey(workspace, JCRContentUtils.escapeNodePath(path),
                hsRequest.getParameter("v"), hsRequest.getParameter("l"), StringUtils.defaultIfEmpty(
                hsRequest.getParameter("t"), StringUtils.EMPTY), hsResponse) : null;
    }

    private boolean isValidNode(JCRNodeWrapper n) throws ValueFormatException, PathNotFoundException,
            RepositoryException {
        n.getSession().getNode(getDataSource(n).getMountPointPath()).getParent();
        if (!Constants.LIVE_WORKSPACE.equals(n.getSession().getWorkspace().getName())) {
            // we check validity only in live workspace
            return true;
        }
        // the file node should be published and visible
        return (!n.hasProperty("j:published") || n.getProperty("j:published").getBoolean())
                && VisibilityService.getInstance().matchesConditions(n);
    }

    private AmazonS3DataSource getDataSource(JCRNodeWrapper node) {
        if (node.getProvider() instanceof S3ExternalStoreProvider) {
            S3ExternalStoreProvider provider = (S3ExternalStoreProvider) node.getProvider();
            return (AmazonS3DataSource) provider.getDataSource();
        }
        return null;
    }

}
