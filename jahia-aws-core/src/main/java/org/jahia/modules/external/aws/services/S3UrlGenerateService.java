/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.services;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.jahia.modules.external.aws.permission.S3UrlGenerator;
import org.jahia.modules.external.aws.providers.AmazonS3DataSource;
import org.jahia.modules.external.aws.providers.S3ExternalStoreProvider;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.content.JCRNodeWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Hashtable;
import java.util.Set;

/**
 * Created by Alexander Storozhuk on 10.07.2015.
 * Time: 12:50
 */
public class S3UrlGenerateService {

    public static final Logger logger = LoggerFactory.getLogger(S3UrlGenerateService.class);

    private Hashtable<String, S3UrlGenerator> urlGeneratorsTable;


    /**
     * Get Url Generator
     *
     * @param node - node of file for generating s3 url
     * @return url Generator for S3 files
     */
    public S3UrlGenerator getUrlGenerator(JCRNodeWrapper node) {
        AmazonS3DataSource dataSource = getDataSource(node);
        if (null != dataSource && null != urlGeneratorsTable) {
            return urlGeneratorsTable.get(dataSource.getMountPointPath());
        }
        return null;
    }

    /**
     * Init  Url Generator
     *
     * @param dataSource Amazon S3 data Source
     * @return true if init UrlGenerator was successful
     */
    public boolean initUrlGenerator(AmazonS3DataSource dataSource) {
        try {
            if (null != dataSource) {
                if (null == urlGeneratorsTable) {
                    urlGeneratorsTable = new Hashtable<>();
                    createUrlGenerator(dataSource);
                } else {
                    S3UrlGenerator s3UrlGenerator = urlGeneratorsTable.get(dataSource.getMountPointPath());
                    if (null == s3UrlGenerator)
                        createUrlGenerator(dataSource);
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Problem to init Url generator", ex);
        }
        return false;
    }

    /**
     * Delete Url Generator
     *
     * @param dataSource Amazon S3 data Source
     */
    public void deleteUrlGenerator(AmazonS3DataSource dataSource) {
        if (null != dataSource && null != urlGeneratorsTable) {
            urlGeneratorsTable.remove(dataSource.getMountPointPath());
        }
    }


    private S3UrlGenerator createUrlGenerator(AmazonS3DataSource dataSource) {
        AwsAccess awsAccess = dataSource.getAccessConfig();
        BasicAWSCredentials credentials = new BasicAWSCredentials(awsAccess.getAccessKey(), awsAccess.getSecretKey());
        AmazonS3 s3 = new AmazonS3Client(credentials);
        s3.setRegion(com.amazonaws.regions.Region.getRegion(awsAccess.getRegions()));
        S3UrlGenerator s3UrlGenerator = new S3UrlGenerator(s3, dataSource);
        urlGeneratorsTable.put(dataSource.getMountPointPath(), s3UrlGenerator);
        return s3UrlGenerator;
    }

    private AmazonS3DataSource getDataSource(JCRNodeWrapper node) {
        if (node.getProvider() instanceof S3ExternalStoreProvider) {
            S3ExternalStoreProvider provider = (S3ExternalStoreProvider) node.getProvider();
            return (AmazonS3DataSource) provider.getDataSource();
        }
        return null;
    }

    public Set<String> getMountPoints() {
         return urlGeneratorsTable.keySet();
    }
}
