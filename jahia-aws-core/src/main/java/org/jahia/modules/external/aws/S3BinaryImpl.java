/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;

import javax.jcr.Binary;
import javax.jcr.RepositoryException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by: Boris
 * Date: 9/23/2014
 * Time: 7:10 PM
 */
public class S3BinaryImpl implements Binary {
    private AmazonS3 s3;
    private S3ObjectId id;
    private ObjectMetadata metadata;

    public S3BinaryImpl(AmazonS3 s3, S3ObjectId id, ObjectMetadata metadata) {
        this.s3 = s3;
        this.id = id;
        this.metadata = metadata;
    }

    @Override
    public InputStream getStream() throws RepositoryException {
        GetObjectRequest request = new GetObjectRequest(id);
        return s3.getObject(request).getObjectContent();
    }

    @Override
    public int read(byte[] b, long position) throws IOException, RepositoryException {
        S3ObjectInputStream objectContentInp = null;
        try {
            GetObjectRequest request = new GetObjectRequest(id);
            long endPosition = (metadata.getInstanceLength() - position > b.length ? b.length + position : metadata.getInstanceLength()) - 1;
            request.setRange(position, endPosition);
            S3Object object = s3.getObject(request);
            objectContentInp = object.getObjectContent();
            return objectContentInp.read(b);
        } catch (AmazonClientException e) {
            throw new RepositoryException(e);
        } catch (IOException e) {
            throw e;
        } finally {
            if (objectContentInp != null)
                objectContentInp.close();
        }
    }

    @Override
    public long getSize() throws RepositoryException {
        return metadata.getInstanceLength();
    }

    @Override
    public void dispose() {

    }
}
