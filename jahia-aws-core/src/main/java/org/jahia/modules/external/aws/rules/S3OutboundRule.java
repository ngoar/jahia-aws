/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.rules;

import org.jahia.api.Constants;
import org.jahia.modules.durlrewrite.DynamicUrlRewriteService;
import org.jahia.modules.external.aws.permission.S3UrlGenerator;
import org.jahia.modules.external.aws.services.S3UrlGenerateService;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.settings.SettingsBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.tuckey.web.filters.urlrewrite.OutboundRule;
import org.tuckey.web.filters.urlrewrite.RewrittenOutboundUrl;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

/**
 * Created by Alexander Storozhuk on 08.07.2015.
 * Time: 19:34
 */
public class S3OutboundRule extends OutboundRule {
    public static final String DEFAULT_NAME = "S3 Outbound rule";

    private static final Logger logger = LoggerFactory.getLogger(S3OutboundRule.class);
    @Autowired
    private S3UrlGenerateService urlGenerateService;

    private DynamicUrlRewriteService rewriteService;
    private String characterEncoding;

    public S3OutboundRule() {
        super();
        setName(DEFAULT_NAME);
    }

    public void init() {
        rewriteService.addOutboundRule(this, DynamicUrlRewriteService.SCOPE_BASE);
    }

    @Override
    public void destroy() {
        rewriteService.removeOutboundRule(this, DynamicUrlRewriteService.SCOPE_BASE);
    }

    @Override
    public String getDisplayName() {
        return DEFAULT_NAME;
    }

    @Override
    public boolean initialise(ServletContext servletContext) {
        characterEncoding = SettingsBean.getInstance().getCharacterEncoding();
        return true;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public String getMatchType() {
        return super.getMatchType();
    }

    @Override
    public RewrittenOutboundUrl execute(String url, HttpServletRequest hsRequest, HttpServletResponse hsResponse) throws InvocationTargetException {
        RewrittenOutboundUrl rewrittenUrl = null;
        try {
            FileKey fileKey = FileKey.parseKey(url);
            if(null==fileKey || !Constants.LIVE_WORKSPACE.equals(fileKey.getWorkspace()))
                return null;
            boolean found=false;
            for (String mountPoint : urlGenerateService.getMountPoints()) {
                if (fileKey.getPath().startsWith(mountPoint)) {
                    found=true;
                    break;
                }
            }
            if (!found)
                return null;
            JCRNodeWrapper node = fileKey.getNode();
            if (node == null || !node.isFile())
                return null;

            if (Constants.LIVE_WORKSPACE.equals(node.getSession().getWorkspace().getName())) {
                S3UrlGenerator urlGenerator = urlGenerateService.getUrlGenerator(node);
                if (null != urlGenerator && urlGenerator.isDirectLink()) {

                    URL s3Url = urlGenerator.getPublicUrl(node.getRealNode().getPath(), hsRequest.isSecure());
                    rewrittenUrl = new RewrittenOutboundUrl(s3Url.toString(), false);
                }

            } else if (Constants.EDIT_WORKSPACE.equals(node.getSession().getWorkspace().getName())) {
                //
            }
        } catch (Exception e) {
            logger.error("Cannot get url for file ", e);
        }

        return rewrittenUrl;
    }

    public void setRewriteService(DynamicUrlRewriteService rewriteService) {
        this.rewriteService = rewriteService;
    }

    public DynamicUrlRewriteService getRewriteService() {
        return rewriteService;
    }

}