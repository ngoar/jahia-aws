/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.sitesettings.models;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by naf on 22.06.2015.
 */
public class AwsAccess implements Serializable {

    private static final long serialVersionUID = 4155622692840654554L;

    private String key;
    private String title;
    private String accessKey;
    private String secretKey;
    private Regions regions;
    private String nodeId;
    private boolean readOnly = false;

    public AwsAccess() {
    }

    public AwsAccess(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public AwsAccess withAccessKey(String accessKey) {
        this.accessKey = accessKey;
        return this;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Regions getRegions() {
        return regions;
    }

    public void setRegions(Regions regions) {
        this.regions = regions;
    }

    public String getRegionName() {
        if (null == regions)
            return null;
        return regions.getName();
    }

    public void setRegionName(String region) {
        setRegion(region);
    }

    public void setRegion(String region) {
        if (null != region && region.length() > 0)
            this.regions = Regions.fromName(region.toLowerCase().replace("_", "-"));
        else
            this.regions = null;
    }

    public BasicAWSCredentials createBasicAWSCredentials() {
        return new BasicAWSCredentials(getAccessKey(), getSecretKey());
    }

    public com.amazonaws.regions.Region getRegion() {
        return getRegions() != null ? com.amazonaws.regions.Region.getRegion(getRegions())
                : Regions.getCurrentRegion();
    }

    public Collection<String> getRegionsValues() {
        ArrayList<String> result = new ArrayList<>();
        for (Regions region : Regions.values()) {
            result.add(region.getName());
        }
        return result;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public AmazonS3Client getClient(){
        AmazonS3Client s3 = new AmazonS3Client(createBasicAWSCredentials());
        s3.setRegion(getRegion());
        return s3;
    }
}
