/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.providers;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.util.ISO8601;
import org.apache.xerces.parsers.SAXParser;
import org.jahia.api.Constants;
import org.jahia.modules.external.ExternalData;
import org.jahia.modules.external.ExternalDataSource;
import org.jahia.modules.external.aws.Constant;
import org.jahia.modules.external.aws.S3BinaryImpl;
import org.jahia.modules.external.aws.event.S3Event;
import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.modules.external.aws.permission.S3ACLManager;
import org.jahia.modules.external.aws.services.impl.AwsServiceImpl;
import org.jahia.services.SpringContextSingleton;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.decorator.JCRMountPointNode;
import org.jahia.services.content.nodetypes.ExtendedNodeType;
import org.jahia.services.content.nodetypes.NodeTypeRegistry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Binary;
import javax.jcr.ItemNotFoundException;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.observation.Event;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Created by: Boris
 * Date: 9/22/2014
 * Time: 2:41 PM
 */
public class AmazonS3DataSource implements ExternalDataSource, ExternalDataSource.Initializable, ExternalDataSource.Writable {

    public static final String PARAM_JCR_PATH = "jcrPath";
    public static final String PARAM_MOUNT_POINT_PATH = "mountPointPath";
    public static final String PARAM_ACCESS_KEY_NAME = "accessKeyName";
    public static final String JCR_PRORP_CONTENT = "jcr_prop_content";
    public static final String JCR_PRORP_NODE = "jcr_prop_node";
    public static final String JCR_I18NPRORP_NODE = "jcr_i18nprop_node";
    private static final List<String> JCR_CONTENT_LIST = Arrays.asList(Constants.JCR_CONTENT);
    public static final String JCR_CONTENT_SUFFIX = "/" + Constants.JCR_CONTENT;
    private static final String THUMBNAIL_FOLDER_EXT = "." + Constant.THUMBNAIL + "/";
    private static final Logger logger = LoggerFactory.getLogger(AmazonS3DataSource.class);
    private AwsService awsService;
    private AmazonS3 s3;
    private AwsAccess accessConfig;
    private String accessKeyName;
    private String mountPointPath;
    private String bucket;
    private String prefix = "";
    private boolean isPublic = false;
    private boolean isDirect = false;
    private JCRNodeWrapper mountPoint;
    private HashMap<String, String> eventProp = new HashMap<>();
    private S3ACLManager aclManager;
    private ExternalData rootNode = null;
    private SAXParser saxParser;

    public static final HashSet<String> supportedNodeTypes = new HashSet<String>(Arrays.asList(Constants.JAHIANT_FOLDER, Constants.JAHIANT_FILE));

    public AmazonS3DataSource() {
    }

    public AmazonS3DataSource(AwsService awsService, JCRNodeWrapper mountPoint) throws RepositoryException {
        this.awsService = awsService;
        this.mountPoint = mountPoint;
        this.mountPointPath = mountPoint.getProperty("mountPoint").getNode().getPath() + "/" + StringUtils.removeEnd(mountPoint.getName(), JCRMountPointNode.MOUNT_SUFFIX);
        init();
    }

    public void init() throws RepositoryException {

        if (null == awsService)
            awsService = (AwsService) SpringContextSingleton.getBean(AwsServiceImpl.BEAN_NAME);
        if (null != getMountPoint()) {
            if (null == bucket)
                setBucket(mountPoint.getPropertyAsString(Constant.BUCKET_FIELD));
            if (null == prefix || prefix.length() == 0)
                setPrefix(mountPoint.getPropertyAsString(Constant.PREFIX_FIELD));
            if (null == accessKeyName)
                accessKeyName = mountPoint.getPropertyAsString(Constant.ACCESS_FIELD);
            try {
                isPublic = mountPoint.getProperty(Constant.PUBLIC_FIELD).getBoolean();
            } catch (RepositoryException ignore) {
            }
            try {
                isDirect = mountPoint.getProperty(Constant.DIRECT_FIELD).getBoolean();
            } catch (RepositoryException ignore) {
            }
        }
        accessConfig = awsService.getAccessConfig(accessKeyName);
        eventProp.put(PARAM_ACCESS_KEY_NAME, accessKeyName);
        eventProp.put(PARAM_MOUNT_POINT_PATH, mountPointPath);
        eventProp.put(Constant.BUCKET_FIELD, getBucket());
        eventProp.put(Constant.PREFIX_FIELD, getPrefix());

    }

    @Override
    public List<String> getChildren(String path) throws RepositoryException {
        List<String> result = getChildren(path, false);
        return result;
    }

    public List<String> getChildren(String path, boolean isMove) throws RepositoryException {
        String s3Path = path2s3(path);
        boolean isThumbnail = isThumbnail(path);
        if (!isMove) {
            if (path.endsWith("/" + Constants.JCR_CONTENT)) {
                return Collections.EMPTY_LIST;
            }
            if (path.length() != 0 && !"/".equals(path) && !isThumbnail) {
                try {
                    execS3Metadata(s3Path);
                    return JCR_CONTENT_LIST;
                } catch (AmazonServiceException e) {
                    if (e.getStatusCode() == 404) { // May be folder
                        s3Path = s3Path + "/";
                    } else {
                        throw e;
                    }
                }
            }
        }
        ListObjectsRequest request = new ListObjectsRequest()
                .withBucketName(bucket)
                .withPrefix(s3Path)
                .withDelimiter("/");
        List<String> result = null;
        try {
            ObjectListing objectListing = getS3ObjectList(request);
            result = new ArrayList<>();
            for (S3ObjectSummary object : objectListing.getObjectSummaries()) {
                String key = object.getKey();
                if (key.length() == 0 || key.length() <= s3Path.length()) continue; // not the same folder
                result.add(key.substring(s3Path.length()));
            }
            for (String folder : objectListing.getCommonPrefixes()) {
                //fix folder with empty name
                if (folder.length() == 0) continue;
                //same folder or some strange
                if (folder.length() <= s3Path.length()) continue;
                // not the thumbnail folder
                if (!isMove && folder.endsWith(THUMBNAIL_FOLDER_EXT)) continue;

                result.add(folder.substring(s3Path.length(), folder.length() - 1));
            }
            if (!isMove && isThumbnail) {
                request.setPrefix(request.getPrefix() + THUMBNAIL_FOLDER_EXT);
                objectListing = getS3ObjectList(request);
                int prefLen = request.getPrefix().length();
                for (S3ObjectSummary object : objectListing.getObjectSummaries()) {
                    String key = object.getKey();
                    if (key.length() <= prefLen) continue;
                    result.add(key.substring(prefLen));
                }
                result.add(Constants.JCR_CONTENT);
            }
        } catch (AmazonServiceException e) {
            if (e.getStatusCode() != 404) { //ignore not found exception
                throw new RepositoryException(e);
            }
        }
        return result;
    }

    @Override
    public ExternalData getItemByIdentifier(String identifier) throws ItemNotFoundException {
        try {
            return getItemByPath(identifier);
        } catch (PathNotFoundException e) {
            throw new ItemNotFoundException(e);
        }
    }

    @Override
    public ExternalData getItemByPath(String jcrPath) throws PathNotFoundException {
        return getItemByPath(jcrPath, true);
    }

    public ExternalData getItemByPath(String jcrPath, boolean isJCR) throws PathNotFoundException {
        if ("/".equals(jcrPath) && null != rootNode)
            return rootNode;
        int index = jcrPath.lastIndexOf("/");
        if (index > 1 && index < jcrPath.length() - 1) {
            String name = jcrPath.substring(index);
            if (!JCR_CONTENT_SUFFIX.equals(name) && name.startsWith("j:"))
                throw new PathNotFoundException();
        }
        String path = path2s3(jcrPath);
        String jcrType;
        ExternalData externalData;
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            ObjectMetadata objectMetadata = null;
            Map<String, String[]> properties = new HashMap<String, String[]>();
            if (isJCR && (path.endsWith(JCR_CONTENT_SUFFIX) ||
                    (jcrPath.contains(Constant.THUMBNAIL) || isThumbnail(jcrPath.substring(0, jcrPath.lastIndexOf('/')))))
                    ) {
                if (path.endsWith(JCR_CONTENT_SUFFIX)) {
                    path = path.substring(0, path.length() - JCR_CONTENT_SUFFIX.length());
                } else {
                    String resourceName = path.substring(path.lastIndexOf('/'));
                    path = path.substring(0, path.lastIndexOf('/'));
                    if (!path.endsWith("." + Constant.THUMBNAIL + resourceName))
                        path = path.concat(".").concat(Constant.THUMBNAIL).concat(resourceName);
                }
                objectMetadata = getS3Client().getObjectMetadata(bucket, path);
                properties.putAll(json2prop(objectMetadata.getUserMetadata().get(JCR_PRORP_CONTENT)));
                properties.put(Constants.JCR_MIMETYPE, new String[]{objectMetadata.getContentType()});
                externalData = new ExternalData(jcrPath, jcrPath, Constants.JAHIANT_RESOURCE, properties);
                Map<String, Binary[]> binaryProperties = new HashMap<String, Binary[]>(1);
                binaryProperties.put(Constants.JCR_DATA, new Binary[]{new S3BinaryImpl(getS3Client(), new S3ObjectId(getBucket(), path), objectMetadata)});
                externalData.setBinaryProperties(binaryProperties);
            } else {
                try {
                    objectMetadata = getS3Client().getObjectMetadata(bucket, path);
                    if (path.length() == 0 || path.endsWith("/"))
                        jcrType = Constants.JAHIANT_FOLDER;
                    else
                        jcrType = Constants.JAHIANT_FILE;
                } catch (AmazonServiceException e) {
                    if (e.getStatusCode() == 404 && !path.endsWith("/")) {
                        // May be folder
                        try {
                            objectMetadata = getS3Client().getObjectMetadata(bucket, path + "/");
                            jcrType = Constants.JAHIANT_FOLDER;
                        } catch (AmazonServiceException e1) {
                            if (e1.getStatusCode() == 404) {
                                ObjectListing listing = getS3Client().listObjects(bucket, path + "/");
                                if (listing.getObjectSummaries().size() > 0 || listing.getCommonPrefixes().size() > 0)
                                    jcrType = Constants.JAHIANT_FOLDER;
                                else
                                    throw e1;
                            } else {
                                throw e1;
                            }
                        }
                    } else {
                        throw e;
                    }
                }
                if (null != objectMetadata && objectMetadata.getLastModified() != null) {
                    properties.putAll(json2prop(objectMetadata.getUserMetadata().get(JCR_PRORP_NODE)));
                    properties.put(Constants.JCR_LASTMODIFIED, formatDate(objectMetadata.getLastModified()));
                }
                externalData = new ExternalData(jcrPath, jcrPath, jcrType, properties);
                if (objectMetadata.getUserMetadata().containsKey(JCR_I18NPRORP_NODE)) {

                    externalData.setI18nProperties(json2i8nProp(objectMetadata.getUserMetadata().get(JCR_I18NPRORP_NODE)));
                }

            }
            if (null == rootNode && "/".equals(jcrPath))
                rootNode = externalData;

            return externalData;
        } catch (AmazonS3Exception ex) {
            throw new PathNotFoundException(ex);
        } finally {
            if (contextClassLoader != null) {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }
    }

    @Override
    public Set<String> getSupportedNodeTypes() {
        return supportedNodeTypes;
    }

    @Override
    public boolean isSupportsHierarchicalIdentifiers() {
        return true;
    }

    @Override
    public boolean isSupportsUuid() {
        return false;
    }

    @Override
    public boolean itemExists(String path) {
        path = path2s3(path);
        try {
            execS3Metadata(path);
            return true;
        } catch (AmazonServiceException e) {
            if (e.getStatusCode() == 404) { // May be folder
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public void start() {
        if (null == accessConfig)
            try {
                accessConfig = awsService.getAccessConfig(accessKeyName);
                initS3();

            } catch (RepositoryException e) {
                logger.error("Can't get AWS access for key " + accessKeyName, e);
            }
    }

    private void initS3() {
        s3 = new AmazonS3Client(accessConfig.createBasicAWSCredentials());
//        s3.setRegion(accessConfig.getRegion());
        aclManager = new S3ACLManager(s3, getBucket());
    }

    private AmazonS3 getS3Client() {
        if (null == s3)
            initS3();
        return s3;
    }

    private void execS3Metadata(String path) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            getS3Client().getObjectMetadata(bucket, path);
        } finally {
            if (contextClassLoader != null) {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }
    }

    private ObjectListing getS3ObjectList(ListObjectsRequest request) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            return getS3Client().listObjects(request);
        } finally {
            if (contextClassLoader != null) {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }
    }

    @Override
    public void stop() {
    }

    @Override
    public void move(String oldPath, String newPath) throws RepositoryException {
        S3Event event = new S3Event(this, Event.NODE_MOVED, eventProp).withParam(PARAM_JCR_PATH, path2jcr(newPath));
        if (oldPath.equals(newPath))
            return;
        ExternalData oldObject = getItemByPath(oldPath, false);
        if (Constants.JAHIANT_FOLDER.equals(oldObject.getType())) {
            oldPath += oldPath.endsWith("/") ? "" : "/";
            newPath += newPath.endsWith("/") ? "" : "/";
        }
        if (!itemExists(newPath) || Constants.JAHIANT_FOLDER.equals(oldObject.getType())) {
            try {
                String s3OldPath = path2s3(oldPath);
                CopyObjectRequest copyObjectRequest = new CopyObjectRequest(getBucket(), s3OldPath, getBucket(), path2s3(newPath));
                ObjectMetadata objectMetadata = getS3Client().getObjectMetadata(bucket, s3OldPath);
                copyObjectRequest.setNewObjectMetadata(objectMetadata);
                CopyObjectResult result = getS3Client().copyObject(copyObjectRequest);
                result.getVersionId();
                if (Constants.JAHIANT_FOLDER.equals(oldObject.getType())) {
                    List<String> children = getChildren(oldPath, true);
                    for (String child : children) {
                        if (!Constants.JCR_CONTENT.equals(child))
                            move(oldPath + child, newPath + child);
                    }
                }
                if (isThumbnail(oldPath)) {
                    try {
                        move(oldPath + THUMBNAIL_FOLDER_EXT, newPath + THUMBNAIL_FOLDER_EXT);
                    } catch (PathNotFoundException ignore) {

                    }
                }
                removeItemByPath(oldPath);
                awsService.addEvent(event);
            } catch (AmazonS3Exception ex) {
                throw new PathNotFoundException(ex);
            } catch (AmazonClientException e) {
                throw new RepositoryException(e);
            }
        } else {
            throw new RepositoryException("Object \"" + newPath + "\" already exist");
        }
    }

    @Override
    public void order(String path, List<String> children) throws RepositoryException {
    }

    @Override
    public void removeItemByPath(String path) throws RepositoryException {
        S3Event event = new S3Event(this, Event.NODE_REMOVED, eventProp).withParam(PARAM_JCR_PATH, path2jcr(path));
        try {
            path = path2s3(path);
            if (path.endsWith("/")) { //folder
                List<String> children = getChildren(path, true);
                for (String child : children) {
                    try {
                        removeItemByPath(path.concat(child));
                    } catch (RepositoryException e) {
                        e.printStackTrace();
                    }
                }
            } else if (isThumbnail(path)) {
                List<String> children = getChildren(path, false);
                for (String child : children) {
                    if (Constants.JCR_CONTENT.equals(child))
                        continue;
                    try {
                        removeItemByPath(path.concat(THUMBNAIL_FOLDER_EXT) + child);
                    } catch (RepositoryException e) {
                        e.printStackTrace();
                    }
                }
                removeItemByPath(path.concat(THUMBNAIL_FOLDER_EXT)); //remove thubnail folder
            }
            DeleteObjectsResult res = getS3Client().deleteObjects(new DeleteObjectsRequest(getBucket()).withKeys(path, path + "/"));
            res.getDeletedObjects();
            awsService.addEvent(event);
        } catch (AmazonClientException e) {
            throw new RepositoryException(e);
        }

    }

    @Override
    public void saveItem(ExternalData data) throws RepositoryException {
        String path = data.getPath();
        String jcrTypeName = data.getType();
        S3Event event = new S3Event(this, Event.NODE_ADDED, eventProp).withParam(PARAM_JCR_PATH, path2jcr(path));
        event.withParam(S3Event.JCR_TYPE, jcrTypeName);
        if (null != data.getProperties().get(Constants.JCR_MIMETYPE))
            event.withParam(Constants.JCR_MIMETYPE, data.getProperties().get(Constants.JCR_MIMETYPE)[0]);
        path = path2s3(path);
        if (path.length() == 0) {
            return;
        }
        ExtendedNodeType nodeType = NodeTypeRegistry.getInstance().getNodeType(jcrTypeName);
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(0);
            if (nodeType.isNodeType(Constants.JAHIANT_RESOURCE)) {
                InputStream content;
                PutObjectRequest putObjectRequest;
                content = new ByteArrayInputStream(new byte[0]);
                final Binary[] binaries;
                if (path.endsWith(JCR_CONTENT_SUFFIX) || data.getName().startsWith(Constant.THUMBNAIL)) {
                    try {
                        if (path.endsWith(JCR_CONTENT_SUFFIX)) {
                            path = path.substring(0, path.lastIndexOf('/'));
                            metadata = getS3Client().getObjectMetadata(getBucket(), path);
                            DeleteObjectsResult res = getS3Client().deleteObjects(new DeleteObjectsRequest(getBucket()).withKeys(path, path + "/"));
                            res.getDeletedObjects();
                        } else {
                            //Add thumbnails folder
                            path = path.substring(0, path.lastIndexOf('/')) + THUMBNAIL_FOLDER_EXT;
                            try {
                                getS3Client().getObjectMetadata(getBucket(), path);
                            } catch (AmazonServiceException e) {
                                if (404 == e.getStatusCode()) {
                                    addEmptyContent(metadata, path);
                                } else {
                                    throw e;
                                }
                            }
                            path += data.getName();
                            metadata.getUserMetadata().put(JCR_PRORP_CONTENT, prop2Json(data.getProperties()).toString());

                        }
                    } catch (AmazonClientException e) {
                        e.printStackTrace();
                    }
                    if (data.getProperties().containsKey(Constants.JCR_MIMETYPE))
                        metadata.setContentType(data.getProperties().get(Constants.JCR_MIMETYPE)[0]);
                    binaries = data.getBinaryProperties().get(Constants.JCR_DATA);

                } else {
                    binaries = null;
                }
                if (binaries != null && binaries.length != 0) {
                    Binary binary = binaries[0];
                    metadata.setContentLength(binary.getSize());
                    content = binary.getStream();
                    putObjectRequest =
                            new PutObjectRequest(getBucket(), path, content, metadata);

                } else {
                    putObjectRequest = new PutObjectRequest(getBucket(), path, content, metadata);
                }
                PutObjectResult result = getS3Client().putObject(putObjectRequest);
                result.getVersionId();
            } else if (nodeType.isNodeType(Constants.JAHIANT_FILE)) {
                try {
                    if (path.length() > 0) { //already exists update metadata
                        metadata = getS3Client().getObjectMetadata(getBucket(), path);
                        metadata.getUserMetadata().put(JCR_PRORP_NODE, prop2Json(data.getProperties()).toString());
                        if (null != data.getI18nProperties() && data.getI18nProperties().size() > 0)
                            metadata.getUserMetadata().put(JCR_I18NPRORP_NODE, i8nProp2Json(data.getI18nProperties()).toString());
                        CopyObjectRequest copyObjectRequest = new CopyObjectRequest(getBucket(), path, getBucket(), path)
                                .withNewObjectMetadata(metadata);
                        getS3Client().copyObject(copyObjectRequest);
                        return;
                    }
                } catch (AmazonClientException e) {
                }
                metadata.getUserMetadata().put(JCR_PRORP_NODE, prop2Json(data.getProperties()).toString());
                if (null != data.getI18nProperties() && data.getI18nProperties().size() > 0)
                    metadata.getUserMetadata().put(JCR_I18NPRORP_NODE, i8nProp2Json(data.getI18nProperties()).toString());

                addEmptyContent(metadata, path);
            } else if (nodeType.isNodeType(Constants.JAHIANT_FOLDER)) {
                path += path.endsWith("/") ? "" : "/";
                try {
                    getS3Client().getObjectMetadata(getBucket(), path);
                    return; //already exists
                } catch (AmazonClientException e) {
                }
                metadata.getUserMetadata().put(JCR_PRORP_NODE, prop2Json(data.getProperties()).toString());
                if (null!=data.getI18nProperties() && data.getI18nProperties().size() > 0) {
                    metadata.getUserMetadata().put(JCR_I18NPRORP_NODE, i8nProp2Json(data.getI18nProperties()).toString());
                }
                addEmptyContent(metadata, path);
            }
        } finally {
            if (contextClassLoader != null) {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }
        if (isPublic) {
            aclManager.publishingObject(path);
        }

        awsService.addEvent(event);
    }

    private void addEmptyContent(ObjectMetadata metadata, String path) {
        metadata.setContentLength(0);
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        PutObjectRequest putObjectRequest =
                new PutObjectRequest(getBucket(), path, emptyContent, metadata);
        // Send request to S3 to create folder
        PutObjectResult result = getS3Client().putObject(putObjectRequest);
        result.getVersionId();
    }

    public AwsAccess getAccessConfig() {
        return accessConfig;
    }

    public String getAccessKeyName() {
        return accessKeyName;
    }

    public void setAccessKeyName(String accessKeyName) {
        this.accessKeyName = accessKeyName;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = null != prefix ? prefix : "";
        if (this.prefix.startsWith("/"))
            this.prefix = this.prefix.substring(1);
        if (!this.prefix.endsWith("/") && this.prefix.length() > 0)
            this.prefix += "/";
    }

    public void setAwsService(AwsServiceImpl awsService) {
        this.awsService = awsService;
    }

    public String path2jcr(String path) {
        StringBuilder sb = new StringBuilder(mountPointPath).append(path);
        return sb.toString();
//        if (path.startsWith(mountPointPath+"/"))
//            return path;
//        return mountPointPath + (path.startsWith("/") ? "" : "/") + path.substring(prefix.length());
    }

    public String path2s3(String path) {
        if (path.startsWith("/"))
            path = path.substring(1);
        return prefix + path;
    }

    private String[] formatDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return new String[]{ISO8601.format(calendar)};
    }

    private boolean isThumbnail(String path) {
        if (path.endsWith("/"))
            return false;
//        if (path.contains(THUMBNAIL_FOLDER_EXT))
//            return true;
        path = path2s3(path + THUMBNAIL_FOLDER_EXT);
        ListObjectsRequest request = new ListObjectsRequest()
                .withBucketName(bucket)
                .withPrefix(path);
        ObjectListing objectListing = getS3ObjectList(request);
        if (!objectListing.getObjectSummaries().isEmpty()) {
            return true;
        }
        if (!objectListing.getCommonPrefixes().isEmpty()) {
            return true;
        }
        return false;

    }

    private static JSONObject i8nProp2Json(Map<String, Map<String, String[]>> properties) {
        JSONObject result = new JSONObject();
        for (String name : properties.keySet()) {
            Map<String, String[]> i8nProperty = properties.get(name);
            try {
                result.put(name, prop2Json(properties.get(name)));
            } catch (JSONException ignore) {
            }
        }
        return result;
    }

    private static JSONObject prop2Json(Map<String, String[]> properties) {
        JSONObject result = new JSONObject();
        for (String name : properties.keySet()) {
            String[] property = properties.get(name);
            if (null != property && property.length > 0) {
                try {
                    result.put(name, new JSONArray(property));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private static Map<String, Map<String, String[]>> json2i8nProp(String jsonStr) {
        Map<String, Map<String, String[]>> properties = new HashMap<>();
        if (null == jsonStr || jsonStr.length() == 0)
            return properties;
        JSONObject json = null;
        try {
            json = new JSONObject(jsonStr);
        } catch (JSONException e) {
            return properties;
        }
        Iterator names = json.keys();
        while (names.hasNext()) {
            String name = names.next().toString();
            try {
                properties.put(name, json2prop(json.getJSONObject(name)));
            } catch (JSONException ignore) {
            }
        }
        return properties;
    }

    private static Map<String, String[]> json2prop(String json) {
        try {
            if (null != json && json.length() > 0)
                return json2prop(new JSONObject(json));
        } catch (JSONException ignore) {
        }
        return new HashMap<String, String[]>();
    }

    private static Map<String, String[]> json2prop(JSONObject json) {
        Map<String, String[]> properties = new HashMap<>();
        if (null == json || json.length() == 0)
            return properties;
        try {
            Iterator names = json.keys();
            while (names.hasNext()) {
                String name = names.next().toString();
                JSONArray values = json.getJSONArray(name);
                String[] propVals = new String[values.length()];
                for (int i = 0; i < propVals.length; i++) {
                    propVals[i] = values.optString(i);
                }
                properties.put(name, propVals);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public JCRNodeWrapper getMountPoint() throws RepositoryException {
//        if (null == mountPoint && null != mountPointPath) {
//            try {
//                JCRMountPointNode mountPoint = JCRSessionFactory.getInstance().getCurrentSystemSession("default", null, null)
//                        .getNode(mountPointPath);
//
//                setMountPoint(mountPoint);
//            } catch (RepositoryException e) {
//                e.printStackTrace();
//            }
//        } else {
//                mountPointPath = mountPoint.getVirtualMountPointNode().getPath();
//                        //.getProperty("mountPoint").getNode().getPath();
//        }

        return mountPoint;
    }

    public void setMountPoint(JCRMountPointNode mountPoint) {
        this.mountPoint = mountPoint;
    }


    public void setMountPointPath(String mountPointPath) {
        this.mountPointPath = mountPointPath;
    }

    public String getMountPointPath() {
        return mountPointPath;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public boolean isDirect() {
        return isDirect;
    }
}
