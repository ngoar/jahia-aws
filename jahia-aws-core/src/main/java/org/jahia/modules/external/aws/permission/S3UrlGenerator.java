/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.permission;

import com.amazonaws.AmazonClientException;
import com.amazonaws.Protocol;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import org.jahia.modules.external.aws.providers.AmazonS3DataSource;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Created by Alexander Storozhuk on 27.05.2015.
 * Time: 16:20
 */
public class S3UrlGenerator {

    public static final String DOMAIN = "amazonaws.com";
    public static final String DOMAIN_PREFIX = "s3";
    private AmazonS3 s3;
    private S3ACLManager s3ACLManager;
    private AmazonS3DataSource dataSource;


    public S3UrlGenerator(AmazonS3 s3, AmazonS3DataSource dataSource) {
        this.dataSource = dataSource;
        s3ACLManager = new S3ACLManager(s3, dataSource.getBucket());
    }

    /**
     * Returns public or private limited URL for accessing an Amazon S3 resource, depending on public source.
     *
     * @param path       The path in the specified bucket under which the desired object
     *                   is stored.
     * @param expiration The time at which the returned pre-signed URL will expire.
     * @param secure     @param secure type of the protocol to use: HTTP or HTTPS.
     * @return
     * @throws AmazonClientException
     * @throws MalformedURLException
     */

    public URL generateUrl(String path, Date expiration, boolean secure) throws AmazonClientException, MalformedURLException {
        if (dataSource.isPublic()) {
            return getPublicUrl(path, secure);
        } else
            return getPrivateLimitedUrl(path, expiration);
    }

    /**
     * Returns private limited URL for accessing an Amazon S3 resource.
     *
     * @param path       The path in the specified bucket under which the desired object
     *                   is stored.
     * @param expiration The time at which the returned pre-signed URL will expire.
     * @return URL for accessing an Amazon S3 resource.
     * @throws AmazonClientException
     */
    public URL getPrivateLimitedUrl(String path, Date expiration) throws AmazonClientException {
        path = path.startsWith("/") ? path.substring(1) : path;
        if(null!=dataSource)
        return s3.generatePresignedUrl(dataSource.getBucket(), path, expiration);
        return null;
    }

    /**
     * Returns public URL for accessing an Amazon S3 resource.
     *
     * @param path   The path in the specified bucket under which the desired object
     *               is stored.
     * @param secure type of the protocol to use: HTTP or HTTPS.
     * @return URL for accessing an Amazon S3 resource.
     * @throws MalformedURLException
     */
    public URL getPublicUrl(String path, boolean secure) throws MalformedURLException {
        path = path.startsWith("/") ? path.substring(1) : path;
        URL url = null;
        com.amazonaws.regions.Region region = dataSource.getAccessConfig().getRegion();
        if (null != region)
            url = new URL(secure ? Protocol.HTTPS.toString() : Protocol.HTTP.toString(), DOMAIN_PREFIX + "-" + region.getName() + "." + region.getDomain(), "/" + dataSource.getBucket() + "/" + path);
        else
            url = new URL(secure ? Protocol.HTTPS.toString() : Protocol.HTTP.toString(), DOMAIN_PREFIX + "." + DOMAIN, "/" + dataSource.getBucket() + "/" + path);
        s3ACLManager.publishingObject(path);
        return url;
    }

    public AmazonS3 getS3() {
        return s3;
    }

    public void setS3(AmazonS3 s3) {
        this.s3 = s3;
    }


    public S3ACLManager getS3ACLManager() {
        return s3ACLManager;
    }

    public boolean isDirectLink() {
        return dataSource.isDirect();
    }

    public boolean isPublicSource() {
        return dataSource.isPublic();
    }
}
