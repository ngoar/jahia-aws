/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws;

/**
 * Created by Igor Shabanov on 16.06.2015.
 */
public class Constant {
    public static final String THUMBNAIL = "thumbnail";
    public static final String ACCESS_TITLE_FIELD = "j:title";
    public static final String ACCESS_KEY_FIELD = "j:accessKey";
    public static final String SECRET_KEY_FIELD = "j:secretKey";
    public static final String REGION_FIELD = "j:regions";
    public static final String BUCKET_FIELD = "j:bucket";
    public static final String PREFIX_FIELD = "j:prefix";
    public static final String ACCESS_FIELD = "j:access";
    public static final String PUBLIC_FIELD = "j:public";
    public static final String DIRECT_FIELD = "j:direct";
    public static final String AWS_CONFIG_NODE_PATH = "awsService/accessList";
    public static final String AWS_CONFIG_NODE_TYPE = "aws:access";
    public static final String AWS_CONFIG_LIST_NODE_TYPE = "aws:accessList";
    public static final String AWS_CONFIG_MIXIN = "jmix:awsAccessName";
    public static final String S3_MP_NODE_TYPE = "aws:s3MountPoint";
}
