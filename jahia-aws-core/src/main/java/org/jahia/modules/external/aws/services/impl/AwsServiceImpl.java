/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.services.impl;

import org.jahia.modules.external.aws.AWSHelper;
import org.jahia.modules.external.aws.Constant;
import org.jahia.modules.external.aws.event.S3Event;
import org.jahia.modules.external.aws.event.S3EventPoll;
import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.decorator.JCRMountPointNode;
import org.jahia.services.query.QueryWrapper;

import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.observation.EventListener;
import javax.jcr.query.Query;
import java.util.*;

/**
 * Created by Igor Shabanov on 16.06.2015.
 */
public class AwsServiceImpl implements AwsService {
    private static final long serialVersionUID = 3829196448704846348L;

    private S3EventPoll eventPoll = null;

    public void init() throws RepositoryException {
        if (null == eventPoll) {
            eventPoll = new S3EventPoll();
            eventPoll.start();
        }
    }

    public void destroy() {
        if (null == eventPoll) {
            eventPoll.terminate();
            eventPoll = null;
        }
    }

    private static AwsAccess readConfig(JCRNodeWrapper config) throws RepositoryException {
        AwsAccess awsAccess = new AwsAccess(AWSHelper.calcKey(config));
        awsAccess.setNodeId(config.getIdentifier());
        awsAccess.setTitle(config.getPropertyAsString(Constant.ACCESS_TITLE_FIELD));
        awsAccess.setAccessKey(config.getPropertyAsString(Constant.ACCESS_KEY_FIELD));
        awsAccess.setSecretKey(config.getPropertyAsString(Constant.SECRET_KEY_FIELD));
        awsAccess.setRegion(config.getPropertyAsString(Constant.REGION_FIELD));
        return awsAccess;
    }


    public AwsAccess getAccessConfig(String key) throws RepositoryException {
        return getAccessConfigs(null, null).get(key);
    }

    public AwsAccess getAccessConfig(JCRSessionWrapper session, String key) throws RepositoryException {
        return getAccessConfigs(session, null).get(key);
    }

    public Map<String, AwsAccess> getAccessConfigs(JCRSessionWrapper session) throws RepositoryException {
        return getAccessConfigs(session, null);
    }

    public Map<String, AwsAccess> getAccessConfigs(JCRSessionWrapper session, String site) throws RepositoryException {
        HashMap<String, AwsAccess> accessList = new HashMap<>();
        if (null == session)
            session = JCRSessionFactory.getInstance().getCurrentSystemSession("default", null, null);
        HashSet<String>configInUse=new HashSet<>();
        QueryWrapper query = session.getWorkspace().getQueryManager().createQuery("SELECT * from [" + Constant.AWS_CONFIG_MIXIN + "]", Query.JCR_SQL2);
        for (JCRNodeWrapper config : query.execute().getNodes()) {
            if(config.hasProperty(Constant.ACCESS_FIELD)){
                String accessName=config.getPropertyAsString(Constant.ACCESS_FIELD);
                if(null!=accessName && accessName.length()!=0)
                    configInUse.add(accessName);
            }
        }
            query = session.getWorkspace().getQueryManager().createQuery("SELECT * from [" + Constant.AWS_CONFIG_NODE_TYPE + "]", Query.JCR_SQL2);
        for (JCRNodeWrapper config : query.execute().getNodes()) {
            if (config.isNodeType(Constant.AWS_CONFIG_NODE_TYPE)) {
                if (null == site || config.getPath().startsWith("/sites/" + site + "/")) {
                    AwsAccess awsAccess = readConfig(config);
                    awsAccess.setReadOnly(configInUse.contains(awsAccess.getKey()));
                    accessList.put(awsAccess.getKey(), awsAccess);
                }
            }
        }
        return accessList;
    }

    private JCRNodeWrapper getConfigListNode(JCRSessionWrapper session, String site) throws RepositoryException {
        QueryWrapper query = session.getWorkspace().getQueryManager().createQuery("SELECT * from [" + Constant.AWS_CONFIG_LIST_NODE_TYPE + "]", Query.JCR_SQL2);
        for (JCRNodeWrapper configNodeList : query.execute().getNodes()) {
            if (null == site && configNodeList.getPath().startsWith("/settings/")) {
                return configNodeList;
            }
            if (null != site && configNodeList.getPath().startsWith("/sites/" + site + "/")) {
                return configNodeList;
            }
        }
        return null;
    }

    public List<JCRMountPointNode> getMountpointsNode(JCRSessionWrapper session) throws RepositoryException {
        QueryWrapper query = session.getWorkspace().getQueryManager().createQuery("SELECT * from [" + Constant.S3_MP_NODE_TYPE+ "]", Query.JCR_SQL2);
        ArrayList<JCRMountPointNode> result = new ArrayList<>();
        final NodeIterator nodeIterator = query.execute().getNodes();
        while (nodeIterator.hasNext()) {
            JCRMountPointNode mountPointNode = (JCRMountPointNode) nodeIterator.next();
            if(null!=mountPointNode.getTargetMountPointPath())
                result.add(mountPointNode);
        }

        return result;
    }

    public Map<String, String> getMountpoints(JCRSessionWrapper session) throws RepositoryException {
        HashMap result = new HashMap();
        for (JCRNodeWrapper mountPoint : getMountpointsNode(session)) {
            result.put(mountPoint.getPath(), mountPoint.getPropertyAsString(Constant.ACCESS_FIELD));
        }
        return result;
    }
    public List<String> getMountPointsPathList(JCRSessionWrapper session,boolean mounted) throws RepositoryException {
        List<String> result = new ArrayList<>();
        for (JCRMountPointNode mountPoint : getMountpointsNode(session)) {
            if(!mounted || JCRMountPointNode.MountStatus.mounted.equals(mountPoint.getMountStatus()))
                result.add(mountPoint.getTargetMountPointPath());
        }
        return result;
    }
    public List<String> getMountPointsPathList(JCRSessionWrapper session) throws RepositoryException {
        return getMountPointsPathList(session,false);
    }

    public void addEventListener(EventListener eventListener) {
        eventPoll.addEventHandler(eventListener);
    }

    public void removeEventHandler(EventListener eventListener) {
        eventPoll.removeEventHandler(eventListener);
    }

    public void addEvent(S3Event event) {
        eventPoll.addEvent(event);
    }


}
