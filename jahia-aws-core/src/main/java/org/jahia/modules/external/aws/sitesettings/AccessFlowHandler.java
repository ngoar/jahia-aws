/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.sitesettings;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import org.jahia.modules.external.aws.Constant;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.modules.external.aws.services.impl.AwsServiceImpl;
import org.jahia.services.SpringContextSingleton;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.render.RenderContext;
import org.jahia.utils.i18n.Messages;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.webflow.execution.RequestContext;

import javax.jcr.RepositoryException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Locale;

/**
 * Created by naf on 02.07.2015.
 */
public class AccessFlowHandler implements Serializable {
    private static final long serialVersionUID = 8128893801930680018L;
    private static final String BUNDLE = "resources.jahia-aws";


    public Collection<AwsAccess> getAccessCongigs(RequestContext ctx) throws RepositoryException {
        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getMainResource().getNode().getSession();
        AwsService awsService = (AwsService) SpringContextSingleton.getBean(AwsServiceImpl.BEAN_NAME);
        return awsService.getAccessConfigs(session).values();
    }

    public AwsAccess getAccessCongig(RequestContext ctx, String key) throws RepositoryException {
        AwsAccess awsAccess = null;
        if(null != key && key.length()==0)
            key=null;
        if (null != key) {
            AwsService awsService = (AwsService) SpringContextSingleton.getBean(AwsServiceImpl.BEAN_NAME);
            RenderContext rc = getRenderContext(ctx);
            JCRSessionWrapper session = rc.getMainResource().getNode().getSession();
            awsAccess = awsService.getAccessConfigs(session).get(key);
        }
        if (null == awsAccess) {
            awsAccess = null != key ? new AwsAccess(key) : new AwsAccess();
            awsAccess.setReadOnly(false);
        }
        return awsAccess;
    }

    public boolean saveAccessSettings(MessageContext messageContext,RequestContext ctx) throws RepositoryException {
        Locale locale = LocaleContextHolder.getLocale();
        //// TODO: 13.09.2016 add Error message

        String key = ctx.getRequestParameters().get("key");
        AwsAccess awsAccess = getAccessCongig(ctx, key);
        awsAccess.setRegionName(ctx.getRequestParameters().get("regionName"));
        awsAccess.setAccessKey(ctx.getRequestParameters().get("accessKey"));
        awsAccess.setSecretKey(ctx.getRequestParameters().get("secretKey"));

        if (null == key || null == awsAccess.getRegion()|| null == awsAccess.getAccessKey() || null == awsAccess.getSecretKey()){
            MessageBuilder messageBuilder = new MessageBuilder().error().defaultText( Messages.get(BUNDLE, "aws.errors.name.mandatory", locale));
            messageContext.addMessage(messageBuilder.build());
            return false;
        }
        try {
            AmazonS3Client s3 = awsAccess.getClient();
            s3.listBuckets();
        } catch (AmazonServiceException e) {
            MessageBuilder messageBuilder = new MessageBuilder().error().defaultText(Messages.get(BUNDLE, "aws.errors.access", locale)+" code - "+e.getErrorCode()+", "+e.getErrorMessage());
            messageContext.addMessage(messageBuilder.build());
            return false;
        }


        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getMainResource().getNode().getSession();
        JCRNodeWrapper configNode;
        if (null != awsAccess && null != awsAccess.getNodeId()) {
            configNode = session.getNodeByUUID(awsAccess.getNodeId());
            session.checkout(configNode);
        } else {
            JCRNodeWrapper configNodeList;
            if ("systemsite".equals(rc.getMainResource().getNode().getResolveSite().getName())) {
                configNodeList = session.getNode("/settings/" + Constant.AWS_CONFIG_NODE_PATH);
            } else {
                configNodeList = rc.getMainResource().getNode().getNode(Constant.AWS_CONFIG_NODE_PATH);
            }
            session.checkout(configNodeList);
            configNode = configNodeList.addNode(awsAccess.getKey(), Constant.AWS_CONFIG_NODE_TYPE);
        }
        configNode.setProperty(Constant.ACCESS_KEY_FIELD, awsAccess.getAccessKey());
        configNode.setProperty(Constant.SECRET_KEY_FIELD, awsAccess.getSecretKey());
        configNode.setProperty(Constant.REGION_FIELD, awsAccess.getRegionName());
        session.save();

        return true;
    }

    public boolean removeAccessSettings(RequestContext ctx, String awsAccessName) throws RepositoryException {
        if (null == awsAccessName)
            return false;
        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getMainResource().getNode().getSession();
        AwsAccess awsAccess = null;
        for (AwsAccess awsAccessItem : getAccessCongigs(ctx)) {
            if (awsAccessName.equals(awsAccessItem.getKey())) {
                awsAccess = awsAccessItem;
            }
        }
        //// TODO: 13.09.2016 Error message
        if (null == awsAccess.getNodeId())
            return false;
        JCRNodeWrapper configNode = session.getNodeByUUID(awsAccess.getNodeId());
        JCRNodeWrapper configNodeRoot = configNode.getParent();
        session.checkout(configNodeRoot);
        configNode.remove();
        session.save();
        return true;
    }

    private RenderContext getRenderContext(RequestContext ctx) {
        return (RenderContext) ctx.getExternalContext().getRequestMap().get("renderContext");
    }

}
