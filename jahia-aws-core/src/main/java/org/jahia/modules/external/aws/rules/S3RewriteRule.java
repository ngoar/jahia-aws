/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.rules;

import org.jahia.api.Constants;
import org.jahia.modules.durlrewrite.DynamicUrlRewriteService;
import org.jahia.modules.external.aws.permission.S3UrlGenerator;
import org.jahia.modules.external.aws.services.S3UrlGenerateService;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.settings.SettingsBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.tuckey.web.filters.urlrewrite.RewrittenUrl;
import org.tuckey.web.filters.urlrewrite.Rule;
import org.tuckey.web.filters.urlrewrite.RuleChain;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Alexander Storozhuk on 08.07.2015.
 * Time: 19:34
 */
public class S3RewriteRule implements Rule {

    public static final String DEFAULT_NAME = "S3 Rewrite rule";
    public static final Logger logger = LoggerFactory.getLogger(S3RewriteRule.class);

    @Autowired
    private S3UrlGenerateService urlGenerateService;
    private DynamicUrlRewriteService rewriteService;
    /* Expiration period in minutes */
    private int expirePeriod = 3;
    private String characterEncoding = null;
    private JCRSessionFactory sessionFactory;
    private boolean filter = false;
    private boolean last = false;

    protected String name;
    protected boolean initialised;
    protected final List errors = new ArrayList(5);
    protected boolean valid;
    protected int id;

    public void init() {
        try {
            rewriteService.addRule(this, DynamicUrlRewriteService.SCOPE_BASE);
            characterEncoding = SettingsBean.getInstance().getCharacterEncoding();
            sessionFactory = JCRSessionFactory.getInstance();
        } catch (Exception e) {
            logger.error("Unable to get the logging service instance. Metrics logging will be disabled.");
        }
    }


    @Override
    public RewrittenUrl matches(String url, HttpServletRequest hsRequest, HttpServletResponse hsResponse, RuleChain chain) throws IOException, ServletException, InvocationTargetException {
        if (logger.isDebugEnabled()) {
            if (!initialised) {
                logger.debug("not initialised, skipping");
                return null;
            }
            if (!valid) {
                logger.debug("not valid, skipping");
                return null;
            }
        }
        RewrittenUrlImpl rewrittenUrl = null;
        try {
            FileKey fileKey = FileKey.parseKey(url, hsRequest, hsResponse, characterEncoding);
            if(null==fileKey)
                return null;
            JCRNodeWrapper node = fileKey.getNode();
            if (node == null || !node.isFile())
                return null;

            if (Constants.LIVE_WORKSPACE.equals(node.getSession().getWorkspace().getName())) {
                S3UrlGenerator urlGenerator = urlGenerateService.getUrlGenerator(node);
                if (null != urlGenerator) {
                    Calendar expire = Calendar.getInstance();
                    expire.add(Calendar.MINUTE, expirePeriod);

                    URL s3Url = urlGenerator.generateUrl(node.getRealNode().getPath(), expire.getTime(), hsRequest.isSecure());
                    if (urlGenerator.isPublicSource()) {
                        hsResponse.setHeader("Cache-Control", "max-age=0, no-cache, no-store");
                        hsResponse.addHeader("Pragma", "no-cache");
                    } else {
                        hsResponse.setHeader("Cache-Control", "max-age=" + expirePeriod);
                    }
                    rewrittenUrl = new RewrittenUrlImpl(s3Url.toString());
                }

            } else if (Constants.EDIT_WORKSPACE.equals(node.getSession().getWorkspace().getName())) {
                //
            }

        } catch (Exception e) {
            logger.error("Cannot get url for file ", e);
        }
        return rewrittenUrl;
    }

    @Override
    public RewrittenUrl matches(String url, HttpServletRequest hsRequest, HttpServletResponse hsResponse) throws IOException, ServletException, InvocationTargetException {
        return matches(url, hsRequest, hsResponse, null);
    }

    public int getExpirePeriod() {
        return expirePeriod;
    }

    /**
     * Setting expiry period life for private limited URL
     *
     * @param expirePeriod expiry period for URL in minutes
     */
    public void setExpirePeriod(int expirePeriod) {
        this.expirePeriod = expirePeriod;
    }


    @Override
    public boolean initialise(ServletContext context) {
        initialised = true;
        boolean ok = true;

        if (errors.size() > 0) {
            ok = false;
        }
        valid = ok;

        return ok;
    }

    @Override
    public void destroy() {
        rewriteService.removeRule(this);
    }

    @Override
    public String getName() {
        if (null != name)
            return name;
        return DEFAULT_NAME;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    /**
     * Is this rule last?.
     *
     * @return boolean
     */
    @Override
    public boolean isLast() {
        return last;
    }

    /**
     * Set to type. note, it will default to false.
     *
     * @param last true or false
     */
    public void setToLast(boolean last) {
        this.last = last;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Will get the rule's id.
     *
     * @return int
     */
    @Override
    public int getId() {
        return id;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public boolean isFilter() {
        return filter;
    }

    /**
     * Will get the list of errors.
     *
     * @return the list of errors
     */
    @Override
    public List getErrors() {
        return errors;
    }

    protected void addError(String s) {
        errors.add(s);
        logger.error(s);
    }

    public void setRewriteService(DynamicUrlRewriteService rewriteService) {
        this.rewriteService = rewriteService;
    }

    public DynamicUrlRewriteService getRewriteService() {
        return rewriteService;
    }
}
