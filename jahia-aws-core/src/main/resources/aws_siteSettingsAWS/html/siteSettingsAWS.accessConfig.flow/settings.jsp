<%@include file="../siteSettingsAWS.base.jsp"%>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>

<%@include file="../i18n.inc" %>
<template:addResources>
    <script type="text/javascript">
        function submitForm(act) {
            $('#accessConfFormAction').val(act);
            $.fancybox.showLoading();
            $('#accessConfForm').submit();
        }
    </script>
</template:addResources>
<div class="row-fluid">
    <div class="span2">
        <div class="buttons">
            <button id="addMailChimpList" type="button" class="btn"
                    onclick="submitForm('back')">
                <i class="icon-backward"> </i><fmt:message key="aws.bask2list"/></button>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="box-1">

    <form action="${flowExecutionUrl}" method="POST" id="accessConfForm">
        <input type="hidden" name="_eventId" id="accessConfFormAction" value="ok"/>
        <fieldset>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <label for="accessKeyName"><fmt:message key="aws.accsess.key"/> <span
                                class="text-error"><c:if test="${ac.key==null}"><strong>*</strong></c:if></span></label>
                        <form:input path="ac.key" id="accessKeyName" readonly="${ac.key!=null}" disabled="${ac.readOnly}"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <label for="AccessKey"><fmt:message key="aws_access.j_accessKey"/> <span
                                class="text-error"><strong>*</strong></span></label>
                        <form:input path="ac.accessKey" id="AccessKey" disabled="${ac.readOnly}"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label for="SecretKey"><fmt:message key="aws_access.j_secretKey"/> <span
                                class="text-error"><strong>*</strong></span></label>
                        <form:input path="ac.secretKey" id="SecretKey" disabled="${ac.readOnly}"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <label for="Region"><fmt:message key="aws_access.j_regions"/> <span
                                class="text-error"><strong>*</strong></span></label>
                        <form:select path="ac.regionName" id="Region" items="${ac.regionsValues}" disabled="${ac.readOnly}"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span1">
                        <div class="buttons">
                            <button class="btn btn-primary" onclick="submitForm('ok')"><fmt:message key="label.save"/></button>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>