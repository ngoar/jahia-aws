<%@include file="../siteSettingsAWS.base.jsp"%>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="accessList" type="org.jahia.modules.external.aws.sitesettings.models.AwsAccess"--%>

<%@include file="../i18n.inc"%>
<template:addResources>
    <script type="text/javascript">
        function submitAccessListForm(act, accessKeyName) {
            $('#accessFormAction').val(act);
            if (accessKeyName) {
                $('#accessKeyName').val(accessKeyName);
            }
//            $.fancybox.showLoading();
            $('#accessForm').submit();
        }
    </script>
</template:addResources>

<form action="${flowExecutionUrl}" method="post" style="display: inline;" id="accessForm">
    <input type="hidden" name="_eventId" id="accessFormAction"/>
    <input type="hidden" name="accessKeyName" id="accessKeyName"/>
</form>
<h2><fmt:message key="aws.access.config.title"/></h2>
<%--<h2><fmt:message key="aws.manage.info"/>  <a href="https://console.aws.amazon.com/cloudfront/home?#distributions:" target="_blank">Management Console</a></h2>--%>
<%@ include file="errors.jspf" %>

<div class="box-1">

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th width="3%">#</th>
            <th><fmt:message key="aws.accsess.key"/></th>
            <th><fmt:message key="aws_access.j_accessKey"/></th>
            <th><fmt:message key="aws_access.j_regions"/></th>
            <th width="150"><fmt:message key="label.actions"/></th>
        </tr>
        </thead>
        <tbody>
        <c:if test="${empty accessList }">
            <tr>
                <td colspan="7"><fmt:message key="label.noItemFound"/></td>
            </tr>
        </c:if>
        <c:forEach items="${accessList}" var="acConf" varStatus="loopStatus">
            <tr>
                <td>${loopStatus.count}</td>
                <td>${acConf.key}</td>
                <td>${acConf.accessKey}</td>
                <td>${acConf.regions.name}</td>
                <td>
                    <c:if test="${!acConf.readOnly}">
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#edit"
                       onclick="submitAccessListForm('edit', '${acConf.key}');return false;">
                        <i class="icon-pencil"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}"
                       href="#delete"
                       onclick="if (confirm('${i18nRemoveConfirm}')) {submitAccessListForm('remove', '${acConf.key}');} return false;">
                        <i class="icon-remove icon-white"></i>
                    </a>
                    </c:if>
                    <c:if test="${acConf.readOnly}"><fmt:message key="aws.inuse"/></c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <div class="buttons">
        <button id="addTemplate" type="button" class="btn btn-primary"
                onclick="submitAccessListForm('add','')">
            <i class="icon-plus icon-white"> </i><fmt:message key="label.add"/></button>
        <div class="clear"></div>
    </div>
</div>