/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.durlrewrite.impl;

import org.apache.commons.lang.ArrayUtils;
import org.jahia.api.RewriteCallback;
import org.jahia.modules.durlrewrite.DynamicUrlRewriteService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.ServletContextAware;
import org.tuckey.web.filters.urlrewrite.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Map;

/**
 * Created by Boris on 01/07/2015.
 */
public class DynamicUrlRewriteServiceImpl implements org.jahia.modules.durlrewrite.DynamicUrlRewriteService, InitializingBean, ServletContextAware, RewriteCallback {
    ServletContext servletContext;
    Map<String, Conf> confs = Collections.unmodifiableMap(ArrayUtils.toMap(new Object[][]{
            {DynamicUrlRewriteService.SCOPE_BASE, new Conf()},
            {DynamicUrlRewriteService.SCOPE_SEO, new Conf()},
            {DynamicUrlRewriteService.SCOPE_LAST, new Conf()},

    }));

    @Override
    public void addRule(Rule rule, String scope) {
        rule.initialise(servletContext);
        getConf(scope).addRule(rule);
    }

    private Conf getConf(String scope) {
        Conf conf = confs.get(scope);
        if (scope == null)
            throw new IllegalArgumentException("Wrong scope " + scope);
        return conf;
    }

    @Override
    public void addOutboundRule(OutboundRule rule, String scope) {
        rule.initialise(servletContext);
        getConf(scope).addOutboundRule(rule);
    }

    @Override
    public void removeRule(Rule rule) {
        for (Conf conf : confs.values()) {
            conf.getRules().remove(rule);
        }
    }

    @Override
    public void removeOutboundRule(OutboundRule rule, String scope) {
        for (Conf conf : confs.values()) {
            conf.getOutboundRules().remove(rule);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (Conf conf : confs.values()) {
            conf.initialise();
        }
    }


    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }


    @Override
    public boolean processRequest(String scope, final HttpServletRequest hsRequest, final HttpServletResponse hsResponse,
                                  FilterChain parentChain) throws IOException, ServletException {
        UrlRewriteEngine urlRewriteEngine = new UrlRewriteEngine(getConf(scope));
        boolean b = urlRewriteEngine.processRequest(hsRequest, hsResponse, parentChain);
        parentChain.doFilter(hsRequest, hsResponse);
        return true;
    }

    @Override
    public String rewriteOutbound(String scope, String url, HttpServletRequest request,
                                  HttpServletResponse response) throws IOException, ServletException, InvocationTargetException {
        UrlRewriteEngine urlRewriteEngine = new UrlRewriteEngine(getConf(scope));
        String rewriteOutbound = urlRewriteEngine.rewriteOutbound(url, request, response);
        return rewriteOutbound;
    }

    private class UrlRewriteEngine extends UrlRewriter {

        public UrlRewriteEngine(Conf conf) {
            super(conf);
        }

        public String rewriteOutbound(String url, HttpServletRequest request,
                                      HttpServletResponse response) throws IOException, ServletException,
                InvocationTargetException {

            RewrittenOutboundUrl rou = processEncodeURL(response, request, false, url);
            if (rou == null)
                return null;
            return rou.getTarget();
        }
    }


}
