<%@include file="../siteSettingsET.base.jsp"%>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="etNodeList" type="org.jahia.services.content.JCRNodeWrapper"--%>

<%@include file="../i18n.inc"%>

<template:addResources>
    <script type="text/javascript">
        function submitFolderForm(act, folderPath) {
            $('#folderFormAction').val(act);
            if (folderPath) {
                $('#folderFormSelected').val(folderPath);
            }
            $.fancybox.showLoading();
            $('#folderForm').submit();
        }
    </script>
</template:addResources>
<form action="${flowExecutionUrl}" method="post" style="display: inline;" id="folderForm">
    <input type="hidden" name="_eventId" id="folderFormAction"/>
    <input type="hidden" name="folderFormSelected" id="folderFormSelected"/>
</form>

<h2><fmt:message key="aws.et.manage.info"/> <a href="https://console.aws.amazon.com/elastictranscoder/home" target="_blank">Management Console</a></h2>


<div class="box-1">
    <%@include file="../messages.inc" %>
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th width="3%">#</th>
            <%--<th><fmt:message key="label.name"/></th>--%>
            <th><fmt:message key="aws.et.folderPath"/></th>
            <th><fmt:message key="aws.et.pipelineId"/></th>
            <th><fmt:message key="aws.et.presetsCount"/></th>
            <th><fmt:message key="aws.et.status"/></th>
            <th width="120"><fmt:message key="label.actions"/></th>
        </tr>
        </thead>
        <tbody>
        <c:set value="${jcr:getChildrenOfType(etNodeList,'aws:folderConfig')}" var="etConfigs"/>
        <c:if test="${empty etConfigs }">
            <tr>
                <td colspan="5"><fmt:message key="label.noItemFound"/></td>
            </tr>
        </c:if>
        <c:forEach items="${etConfigs}" var="etConf" varStatus="loopStatus">
            <c:set value="0" var="presets"/>
            <c:forEach items="${jcr:getNodes(etConf,'aws:presets')}" var="presetsNode"  varStatus="ploopStatus">
                <c:set value="${fn:length(jcr:getNodes(presetsNode,'aws:preset'))}" var="presets"/>
            </c:forEach>
            <tr>
                <td>${loopStatus.count}</td>
                <td>${etConf.properties['j:folderS3Path'].string}</td>
                <td>
                <a href="https://console.aws.amazon.com/elastictranscoder/home?#pipeline-details:${etConf.properties['j:pipelineId'].string}"
                   target="_blank"> ${etConf.properties['j:pipelineId'].string}</a>
                </td>
                <td>${presets}</td>

                <td>
                    <img title="<fmt:message key='aws.et.${etConf.properties["j:active"].boolean ? "active" : "paused"}'/>"
                         src="<c:url value="${url.currentModule}/icons/${etConf.properties['j:active'].boolean ? 'on-success' : 'off-error'}-icon.png"/>"
                         width="25" height="25" onclick="submitFolderForm('switchFolderSettings', '${etConf.properties['j:folderS3Path'].string}');">
                </td>

                <td>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#edit"
                       onclick="submitFolderForm('editFolderSettings', '${etConf.properties['j:folderS3Path'].string}');return false;">
                        <i class="icon-pencil"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}"
                       href="#delete"
                       onclick="if (confirm('${i18nRemoveConfirm}')) {submitFolderForm('removeFolderSettings', '${etConf.properties['j:folderS3Path'].string}');} return false;">
                        <i class="icon-remove icon-white"></i>
                    </a>
                </td>

            </tr>
        </c:forEach>

        </tbody>
    </table>

    <div class="buttons">
        <button id="addTemplate" type="button" class="btn btn-primary"
                onclick="submitFolderForm('addFolderSettings')">
            <i class="icon-plus icon-white"> </i><fmt:message key="label.add"/></button>
        <div class="clear"></div>
    </div>
</div>