<%@include file="../siteSettingsET.base.jsp" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="etConfig" type="org.jahia.modules.external.aws.et.AwsETFolderConfig"--%>

<template:addResources>
    <script type="text/javascript">
        function submitForm(act, presetKey) {
            $('#formAction').val(act);
            if (presetKey)
                $('#presetKey').val(presetKey);
            $('#elasticTranscoderForm').submit();
        }
        function switchActive() {
            $('#etFolderActiveMsg').toggle();
            $('#etFolderPausedMsg').toggle();
            return true;
        }
    </script>
    <script type="text/javascript">
        function fancyboxShowActivity(fieldId){
            $("#"+fieldId+"-treeItemSelector").bind("add",function() {
                $.fancybox.hideLoading();
                $(".hasChildren.expandable").find( "span" ).bind("click",function() {$.fancybox.showLoading();});
                $(".hasChildren.expandable").find( "div" ).bind("click",function() {$.fancybox.showLoading();});
            });
        }

        function treeSelect() {
            var mpPath = $('#etMpPath').val();
            jahiaCreateTreeItemSelector("etJCRPath", "etFolderPath", "/cms/render/default/en",mpPath,
                    "jnt:virtualsitesFolder,jmix:accessControlled,jnt:folder,jnt:mounts", "jnt:folder,jnt:mounts", "path",
                    function() {$.fancybox.hideLoading(); return true;}, function() {$.fancybox.hideLoading();}, null,
                    { onUpdate : function(){$.fancybox.showLoading();fancyboxShowActivity("etJCRPath");}}
            );

        };
        function setS3Path() {
            var mpPath = $('#etMpPath').val();
            var jcrPath = $('#etJCRPath').val();
            var s3path = '';
            if ((jcrPath.length > mpPath.length)
                    && (jcrPath.substring(0, mpPath.length + 1) === (mpPath + "/"))
            ) {
                jcrPath.substring(mpPath.length + 1)
            }

            $('#etFolderPath').attr('value', s3path);
        }
        function setJcrPath() {
            $('#etFolderPath').attr('value','');
            $('#etJCRPath').attr('value', $('#etMpPath').val());
        }

    </script>
</template:addResources>

<div class="row-fluid">
    <div class="span2">
        <div class="buttons">
            <button id="addMailChimpList" type="button" class="btn"
                    onclick="submitForm('back')">
                <i class="icon-backward"> </i>&nbsp;<fmt:message key="aws.et.back"/></button>
            <div class="clear"></div>
        </div>
    </div>
</div>

<%@include file="../messages.inc" %>
<%-- for assets init --%>
<div style="display:none">
    <ui:folderSelector root="/sites" fieldId="etJCRPath" displayFieldId="etFolderPath"
                       includeChildren="false" displayIncludeChildren="false" valueType="path"
                       selectableNodeTypes="jnt:folder" nodeTypes="jnt:folder"/>
</div>

<form action="${flowExecutionUrl}" method="POST" id="elasticTranscoderForm">
    <div class="box-1">
        <input type="hidden" name="_eventId" id="formAction" value="ok"/>
        <fieldset>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span1">
                        <label for="etFolderActive"><fmt:message key="aws.et.status"/></label>
                        <form:checkbox path="etConfig.active" id="etFolderActive" value="etConfig.active"
                                       onclick="switchActive();"/>&nbsp;
                        <span id="etFolderActiveMsg"
                              style="color: green; <c:if test="${!etConfig.active}">display: none;</c:if>"><fmt:message
                                key="aws.et.active"/></span>
                        <span id="etFolderPausedMsg"
                              style="color: red;   <c:if test="${etConfig.active}">display: none;</c:if>"><fmt:message
                                key="aws.et.paused"/></span>
                    </div>
                    <div class="span2">
                        <label for="etJCRPath"><fmt:message key="aws.et.jcrPath"/></label>
                        <form:input path="etConfig.jcrPath" id="etJCRPath" readonly="true" onchange="setS3Path();" cssStyle="width: auto;"/>
                    </div>
                    <div class="span2">
                        <label for="etMpPath"><fmt:message key="aws.et.mountPoint"/><span class="text-error"><strong>*&nbsp;</strong></span></label>
                        <form:select path="etConfig.mountPoint" items="${etConfig.mountPointsList}" id="etMpPath"
                                     onchange="setJcrPath();submitForm('reload');" cssStyle="width: auto;"/>
                    </div>
                    <div class="span2">
                        <label for="etFolderPath"><fmt:message key="aws.et.s3FolderPath"/><span class="text-error"><strong>*&nbsp;</strong></span>
                            <a href="#etJCRPath-treeItemSelector" id="etJCRPath-treeItemSelectorTrigger" onclick="treeSelect();">Select folder</a></label>
                        <form:input path="etConfig.s3path" id="etFolderPath" readonly="true" onchange="setS3Path();" cssStyle="width: auto;"/>

                    </div>

                    <div class="span2">
                        <label for="etPipelineId"><fmt:message key="aws.et.pipelineId"/><span class="text-error"><strong>*</strong></span>
                            <c:if test="${fn:length(etConfig.pipelineId)==0}">
                                <a href="#createPipeline" onclick="submitForm('createPipline')"><fmt:message key="aws.et.findPipeline"/></a>
                            </c:if>
                        </label>
                        <form:input path="etConfig.pipelineId" id="etPipelineId" cssStyle="width: auto;"/>
                    </div>
                </div>
            </div>
        </fieldset>

    </div>
    <div class="box-1">
        <fieldset>
            <h2><fmt:message key="aws.et.presetsCount"/>&nbsp<a
                    href="https://console.aws.amazon.com/elastictranscoder/home?#presets:" target="_blank"><fmt:message
                    key="aws.et.presetsURL"/></a></h2>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th width="3%">#</th>
                    <th><fmt:message key="aws.et.preset.key"/></th>
                    <th><fmt:message key="aws.et.preset.id"/></th>
                    <th width="25%"><fmt:message key="label.actions"/></th>
                </tr>
                </thead>
                <tbody>
                <%--<c:set value="0" var="presets"/>--%>
                <c:forEach items="${etConfig.presets}" var="preset" varStatus="loopStatus">
                    <%--<c:set value="" var="presets"/>--%>
                    <tr>
                        <td>${loopStatus.index+1}</td>
                        <td>${preset.key}</td>
                        <td>${preset.id}:${preset.name}</td>
                        <td>
                            <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}"
                               href="#delete" onclick="submitForm('remPreset','${preset.key}');">
                                <i class="icon-remove icon-white"></i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                <tr>
                    <td>*</td>
                    <td><input type="text" name="presetKey" id="presetKey" value=""/></td>
                    <%--<td><input type="text" name="presetId" id="presetId" value=""/></td>--%>
                    <td><c:if test="${null != etConfig.presetsList}">
                        <select id="presetId" name="presetId" style="width: auto;" >
                            <c:forEach items="${etConfig.presetsList}" var="preset" varStatus="loopStatus">
                                <option value="${preset.id}" >${preset.id}:${preset.name}</option>
                            </c:forEach>
                        </select>
<%--
                        <form:select path="" items="${etConfig.presetsList}" name="presetId" id="presetId"
                                     cssStyle="width: auto;"/>
--%>
                    </c:if>
                    </td>
                    <td>
                        <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#add"
                           onclick="submitForm('addPreset');">
                            <i class="icon-plus"></i>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </fieldset>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="buttons">
                <button class="btn btn-primary" onclick="submitForm('ok')">
                    <i class="icon-white"></i><fmt:message key="label.save"/>&nbsp;&nbsp;&nbsp;
                </button>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</form>
