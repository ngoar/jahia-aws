/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.et;

import com.amazonaws.services.elastictranscoder.model.Preset;
import org.jahia.services.content.JCRNodeWrapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by naf on 30.06.2015.
 */
public class AwsETFolderConfig implements Serializable {
    private static final long serialVersionUID = 8355858093485921040L;
    private String nodeID;
    private boolean active = false;
    private String mountPoint = null;
    private String s3path;
    private String jcrPath;
    private String pipelineId = null;
    private List<Preset> presets = new ArrayList<>();
    private List<String> mountPointsList;
    private List<Preset> presetsList;

    public AwsETFolderConfig(String jcrPath,List<String> mountPointsList) {
        if(null==jcrPath)
            jcrPath="";
        if (!jcrPath.startsWith("/"))
            jcrPath = "/" + jcrPath;
        if (jcrPath.endsWith("/"))
            jcrPath = jcrPath.substring(0, jcrPath.length() - 1);
        this.jcrPath = jcrPath;
        this.mountPointsList=mountPointsList;
        for (String mp : mountPointsList) {
            if(jcrPath.equals(mp) ||  jcrPath.startsWith(mp+"/")){
                setMountPoint(mp);
                setS3path(jcrPath.equals(mp)?"":jcrPath.substring(mp.length()+1));
                break;
            }
        }
    }

    public List<String> getMountPointsList() {
        return mountPointsList;
    }

    public String getNodeID() {
        return nodeID;
    }

    public void setNodeID(String nodeID) {
        this.nodeID = nodeID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setPipelineId(String pipelineId) {
        this.pipelineId = pipelineId;
    }

    public String getJcrPath() {
        return jcrPath;
    }

    public void setJcrPath(String jcrPath) {
        this.jcrPath = jcrPath;
    }

    public String getPipelineId() {
        return pipelineId;
    }

    public String getMountPoint() {
        return mountPoint;
    }

    public void setMountPoint(String mountPoint) {
        if(mountPoint.endsWith("/"))
            mountPoint=mountPoint.substring(0,mountPoint.length()-1);
        this.mountPoint = mountPoint;
    }

    public String getS3path() {
        return s3path;
    }

    public void setS3path(String s3path) {
        if(s3path.startsWith("/"))
            s3path = s3path.substring(1);
        this.s3path = s3path;
    }

    public List<Preset> getPresets() {
        return presets;
    }

    public void setPresets(List<Preset> presets) {
        this.presets = presets;
    }

    public void addPreset(String key, String id) {
        if(null==key||null==id)
            return;
        for (Preset preset : presetsList) {
            if(preset.getId().equals(id))
                presets.add(preset.withKey(key));
        }

    }

    public boolean removePreset(String key){
        Preset forRemove=null;
        for (Preset preset : presets) {
            if(preset.getKey().equals(key)) {
                forRemove = preset;
                break;
            }
        }
        return null!=forRemove?presets.remove(forRemove):false;
    }

    public Preset getPresetByKey(String key){
        for (Preset preset : presets) {
            if(preset.getKey().equals(key)) {
                return preset;
            }
        }
        return null;
    }

    public boolean addNewPreset(Preset newPreset){
        if(null==newPreset.getKey()||newPreset.getKey().length()==0||null!=getPresetByKey(newPreset.getKey()))
            return false;
        if(null==newPreset.getId()||newPreset.getId().length()==0)
            return false;
        if(null==newPreset.getContainer()||newPreset.getContainer().length()==0)
            return false;
        addPreset(newPreset.getKey(),newPreset.getContainer(),newPreset.getId());
        return true;
    }

    private void addPreset(String key, String container, String id) {
        presets.add(new Preset(key,container,id));
    }

    public void addPreset(JCRNodeWrapper preset) {
        presets.add(new Preset(preset.getPropertyAsString(Preset.KEY),preset.getPropertyAsString(Preset.CONTAINER),preset.getPropertyAsString(Preset.ID)));
    }

    public static class Preset implements Serializable {
        private static final long serialVersionUID = 5980859446511975107L;
        public static final String KEY="j:presetKey";
        public static final String CONTAINER="j:container";
        public static final String ID="j:presetId";
        private String key;
        private String container;
        private String id;
        private String name;


        public Preset(String key,com.amazonaws.services.elastictranscoder.model.Preset preset) {
            this.key=key;
            container=preset.getContainer();
            id=preset.getId();
            name=preset.getName();
        }
        public Preset(String key, String container, String id) {
            this.key = key;
            this.container=container;
            this.id = id;
        }

        public String getKey() {
            return key;
        }

        public String getContainer() {
            return container;
        }

        public void setContainer(String container) {
            this.container = container;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public Preset withName(String name) {
            this.name = name;
            return this;
        }

        public Preset withKey(String key) {
            this.key=key;
            return this;
        }
    }

    public List<Preset> getPresetsList() {
        return presetsList;
    }

    public void setPresetsList(List<Preset> presetsList) {
        this.presetsList = presetsList;
    }


}
