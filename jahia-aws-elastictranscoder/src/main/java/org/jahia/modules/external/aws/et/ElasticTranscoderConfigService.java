/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.et;

import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.services.SpringContextSingleton;
import org.jahia.services.content.JCRNodeIteratorWrapper;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by naf on 11.06.2015.
 */
public class ElasticTranscoderConfigService {

    public static final String CONFIG_NODE_PATH = "/settings/awsService/elasticTranscoder";
    public static final String CONFIG_ROOT_NODE_TYPE = "aws:folderConfigList";
    public static final String CONFIG_NODE_TYPE = "aws:folderConfig";
    public static final String CONFIG_NODE_PATH_FIELD="j:folderS3Path";
    public static final String BEAN_NAME = "ElasticTranscoderConfigService";
    private List<AwsETFolderConfig> folderConfigList = new ArrayList<>();

    private AwsService awsService;

    public void init() throws RepositoryException {
        if (null == awsService)
            awsService = (AwsService) SpringContextSingleton.getBean(AwsService.BEAN_NAME);
        JCRSessionWrapper session = JCRSessionFactory.getInstance().getCurrentSystemSession("default", null, null);
        initConfigByNode(session,getConfigNode(session));

    }

    public JCRNodeWrapper getConfigNode(JCRSessionWrapper session) throws RepositoryException {
        QueryWrapper query = session.getWorkspace().getQueryManager().createQuery("SELECT * from [" + CONFIG_ROOT_NODE_TYPE + "]", Query.JCR_SQL2);
        for (JCRNodeWrapper etFolderConfigs : query.execute().getNodes()) {
                return etFolderConfigs;
        }
        throw new RepositoryException("Can't finde Elastic Transcoder config node!");

    }
    private void initConfigByNode(JCRSessionWrapper sesseion,JCRNodeWrapper node) {
        try {
            for (JCRNodeWrapper nodeFolderConfig : node.getNodes()) {
                if (!nodeFolderConfig.isNodeType(CONFIG_NODE_TYPE)) continue;
                AwsETFolderConfig folderConfig=initByNode(sesseion,nodeFolderConfig);
                folderConfigList.add(folderConfig);
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }

    public AwsETFolderConfig initByNode(JCRSessionWrapper sesseion, JCRNodeWrapper nodeFolderConfig) throws RepositoryException {
        if(null==nodeFolderConfig)
            return null;
        String folder = nodeFolderConfig.getPropertyAsString(CONFIG_NODE_PATH_FIELD);

        AwsETFolderConfig folderConfig = new AwsETFolderConfig(folder,awsService.getMountPointsPathList(sesseion));
        folderConfig.setNodeID(nodeFolderConfig.getIdentifier());
        folderConfig.setActive(nodeFolderConfig.getProperty("j:active").getBoolean());
        folderConfig.setPipelineId(nodeFolderConfig.getPropertyAsString("j:pipelineId"));
        JCRNodeWrapper nodeFolderPresets = nodeFolderConfig.getNode("presets");
        for (JCRNodeWrapper preset : nodeFolderPresets.getNodes()) {
            folderConfig.addPreset(preset);
        }
        return folderConfig;
    }

    public static boolean saveNodeByConfig(JCRSessionWrapper session, JCRNodeWrapper nodeFolderConfig,AwsETFolderConfig folderConfig,List<AwsETFolderConfig> folderConfigList) throws RepositoryException {
        session.checkout(nodeFolderConfig);
        nodeFolderConfig.setProperty(CONFIG_NODE_PATH_FIELD,folderConfig.getJcrPath());
        nodeFolderConfig.setProperty("j:active",folderConfig.isActive());
        nodeFolderConfig.setProperty("j:pipelineId",folderConfig.getPipelineId());
        JCRNodeWrapper presetsNode;
        try {
            presetsNode= nodeFolderConfig.getNode("presets");
        } catch (PathNotFoundException e) {
            presetsNode = nodeFolderConfig.addNode("presets","aws:presets");
        }
        session.checkout(presetsNode);
        List<AwsETFolderConfig.Preset> presets4add=new ArrayList<>();
        presets4add.addAll(folderConfig.getPresets());
        for (JCRNodeWrapper presetNode : presetsNode.getNodes()) {
            if(!presetNode.isNodeType("aws:preset"))
                continue;
            String key = presetNode.getPropertyAsString(AwsETFolderConfig.Preset.KEY);
            AwsETFolderConfig.Preset preset=folderConfig.getPresetByKey(key);
            if(null==preset){
                presetNode.remove();
            }else{
                presetNode.setProperty(AwsETFolderConfig.Preset.ID,preset.getId());
                presets4add.remove(preset);
            }
        }
        for (AwsETFolderConfig.Preset preset : presets4add) {
            JCRNodeWrapper presetNode = presetsNode.addNode(preset.getKey(),"aws:preset");
            presetNode.setProperty(AwsETFolderConfig.Preset.KEY,preset.getKey());
            presetNode.setProperty(AwsETFolderConfig.Preset.CONTAINER,preset.getContainer());
            presetNode.setProperty(AwsETFolderConfig.Preset.ID, preset.getId());
        }

        session.save();
        folderConfigList.add(folderConfig);
        return true;
    }
    public AwsETFolderConfig getConfigByFolder(String jcrPath,JCRSessionWrapper session) throws RepositoryException {
        //todo read from JCR
        if(null==jcrPath)
            return new AwsETFolderConfig("",awsService.getMountPointsPathList(session));
        AwsETFolderConfig result = new AwsETFolderConfig(jcrPath,awsService.getMountPointsPathList(session));
        jcrPath = result.getJcrPath();
        AwsETFolderConfig result1 = null;
        for (AwsETFolderConfig awsETFolderConfig : folderConfigList) {
            if (!awsETFolderConfig.isActive())
                continue;
            if (jcrPath.equals(awsETFolderConfig.getJcrPath()))
                return awsETFolderConfig;

            if (!result.getJcrPath().startsWith(awsETFolderConfig.getJcrPath()))
                continue;
            if (null == result1 || awsETFolderConfig.getJcrPath().length() > result1.getJcrPath().length())
                result1 = awsETFolderConfig;
        }
        return null != result1 ? result1 : result;
    }


    public void setAwsService(AwsService awsService) {
        this.awsService = awsService;
    }

    public AwsService getAwsService() {
        return awsService;
    }

    public List<AwsETFolderConfig> getFolderConfigList() {
        return folderConfigList;
    }


}
