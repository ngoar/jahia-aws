/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.et;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.elastictranscoder.AmazonElasticTranscoderClient;
import com.amazonaws.services.elastictranscoder.model.*;
import org.jahia.modules.external.ExternalContentStoreProvider;
import org.jahia.modules.external.aws.providers.AmazonS3DataSource;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.SpringContextSingleton;
import org.jahia.services.content.JCRContentUtils;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.decorator.JCRMountPointNode;
import org.jahia.services.render.RenderContext;
import org.jahia.utils.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.webflow.execution.RequestContext;

import javax.jcr.RepositoryException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by naf on 26.06.2015.
 */
public class FlowHandler implements Serializable {
    private static final long serialVersionUID = 8497579653074382510L;
    private static final String ET_SOURCE_TYPE = "elasticTranscoderSettrings";
    private static final String BUNDLE = "resources.jahia-aws-elastictranscoder";

    private static final Logger logger = LoggerFactory.getLogger(FlowHandler.class);

    public List<AwsETFolderConfig> getSettings(RequestContext ctx) {
        ElasticTranscoderConfigService config = (ElasticTranscoderConfigService) SpringContextSingleton.getBean(ElasticTranscoderConfigService.BEAN_NAME);
        return config.getFolderConfigList();
    }

    public JCRNodeWrapper getConfigNodeList(RequestContext ctx) {

        ElasticTranscoderConfigService config = (ElasticTranscoderConfigService) SpringContextSingleton.getBean(ElasticTranscoderConfigService.BEAN_NAME);
        RenderContext rc = getRenderContext(ctx);
        try {
            JCRSessionWrapper session = rc.getSite().getSession();
            return config.getConfigNode(session);
        } catch (RepositoryException e) {
            logger.error("Can't get config node list!", e);
        }
        return null;
    }

    public AwsETFolderConfig getFolderSettings(RequestContext ctx, String path) throws RepositoryException {
        ElasticTranscoderConfigService config = (ElasticTranscoderConfigService) SpringContextSingleton.getBean(ElasticTranscoderConfigService.BEAN_NAME);
        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getSite().getSession();
        AmazonS3DataSource dataSource=null;
        AwsETFolderConfig result = new AwsETFolderConfig("", config.getAwsService().getMountPointsPathList(session,true));
        try {
        if (null != path && !"addFolderSettings".equals(ctx.getCurrentEvent().getId()) ) {
            result = config.initByNode(session, getConfigNodeByPath(getConfigNodeList(ctx), path));
            dataSource = getDataSource(ctx, result);
        }
        if(null==dataSource){//if no path we get
            List<JCRMountPointNode> mpList = config.getAwsService().getMountpointsNode(session);
            for (int i = 0; i < mpList.size(); i++) {
                JCRMountPointNode mpNode=config.getAwsService().getMountpointsNode(session).get(i);
                ExternalContentStoreProvider provider = (ExternalContentStoreProvider)mpNode.getMountProvider();
                if(mpNode.getMountStatus().equals(JCRMountPointNode.MountStatus.mounted) && null!=provider) {
                    dataSource = (AmazonS3DataSource) provider.getDataSource();
                    break;
                }
            }

        }
            if(null==dataSource){
                MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
                mb.defaultText(Messages.get(BUNDLE, "aws.et.no.mp", LocaleContextHolder.getLocale()));
                ctx.getMessageContext().addMessage(mb.build());
                return result;
            }

            AmazonElasticTranscoderClient client = new AmazonElasticTranscoderClient(dataSource.getAccessConfig().createBasicAWSCredentials());
            client.setRegion(dataSource.getAccessConfig().getRegion());
            ListPresetsResult pr = client.listPresets();
            HashMap<String,AwsETFolderConfig.Preset> presetsMap=new HashMap<>();
            for (Preset preset : pr.getPresets()) {
                presetsMap.put(preset.getId(),new AwsETFolderConfig.Preset(null,preset));
            }

            result.setPresetsList(new ArrayList<>(presetsMap.values()));
            for (AwsETFolderConfig.Preset preset : result.getPresets()) {
                if(presetsMap.containsKey(preset.getId()))
                    preset.withName(presetsMap.get(preset.getId()).getName());
            }
        } catch (AmazonClientException e) {
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.presetsList.get.fail", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
        }
        return result;
    }

    public boolean removeFolderSettings(RequestContext ctx, String path) throws RepositoryException {
        if (null == path)
            return false;
        ElasticTranscoderConfigService config = (ElasticTranscoderConfigService) SpringContextSingleton.getBean(ElasticTranscoderConfigService.BEAN_NAME);
        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getSite().getSession();
        JCRNodeWrapper configNode = config.getConfigNode(session);
        JCRNodeWrapper configNodeFolder = getConfigNodeByPath(configNode, path);
        if (null != configNodeFolder) {
            session.checkout(configNode);
            configNodeFolder.remove();
            session.save();
        }
        return true;
    }
    public boolean switchFolderSettings(RequestContext ctx, String path) throws RepositoryException {
        if (null == path)
            return false;
        AwsETFolderConfig folderConfig =getFolderSettings(ctx,path);
        folderConfig.setActive(!folderConfig.isActive());
        return saveFolderSettings(ctx, folderConfig);
    }

    public boolean saveFolderSettings(RequestContext ctx, AwsETFolderConfig etConfig) throws RepositoryException {
        boolean correct=true;
        if (null == etConfig)
            return false;
        if (etConfig.getPresets().size()==0) {
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.presets.null", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            correct = false;
        }
        if (null==etConfig.getPipelineId()||etConfig.getPipelineId().length()==0) {
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.pipeline.null", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            correct = false;
        }
        if (null==etConfig.getJcrPath()||etConfig.getJcrPath().length()==0) {
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.jcrPath.null", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            correct = false;
        }
        if(!correct)
            return false;

        ElasticTranscoderConfigService config = (ElasticTranscoderConfigService) SpringContextSingleton.getBean(ElasticTranscoderConfigService.BEAN_NAME);
        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getMainResource().getNode().getSession();
        JCRNodeWrapper configNode = config.getConfigNode(session);
        JCRNodeWrapper configNodeFolder;
        if (null != etConfig.getNodeID()) {
            configNodeFolder = session.getNodeByUUID(etConfig.getNodeID());
        } else {
            configNodeFolder = configNode.addNode(JCRContentUtils.findAvailableNodeName(configNode, "folder"), ElasticTranscoderConfigService.CONFIG_NODE_TYPE);
        }
        return config.saveNodeByConfig(session, configNodeFolder, etConfig, config.getFolderConfigList());
    }

    private JCRNodeWrapper getConfigNodeByPath(JCRNodeWrapper configNode, String path) throws RepositoryException {
        if (null == configNode)
            return null;
        for (JCRNodeWrapper folderConfig : configNode.getNodes()) {
            String nodePath = folderConfig.getPropertyAsString(ElasticTranscoderConfigService.CONFIG_NODE_PATH_FIELD);
            if (path.equals(nodePath)) {
                return folderConfig;
            }
        }
        return null;
    }

    public boolean addPreset(RequestContext ctx, AwsETFolderConfig etConfig, String presetKey, String presetId){

        if(null==presetKey||null==presetId||0==presetId.length()||0==presetKey.length()){
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.preset.fail", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            return false;
        }
        etConfig.addPreset(presetKey,presetId);
        return true;
    }
    private AmazonS3DataSource getDataSource(RequestContext ctx, AwsETFolderConfig etConfig) throws RepositoryException {
        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getMainResource().getNode().getSession();
        String mountPoint = etConfig.getMountPoint();
        if (null == mountPoint || mountPoint.length() == 0) {
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.pipeline.mp.null", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            return null;
        }
        JCRNodeWrapper mpNode = session.getNode(mountPoint);

        if (!(mpNode.getProvider() instanceof ExternalContentStoreProvider)) {
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.pipeline.mp.fail", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            return null;
        }
        ExternalContentStoreProvider provider = (ExternalContentStoreProvider) mpNode.getProvider();
        if (!(provider.getDataSource() instanceof AmazonS3DataSource)) {
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.pipeline.mp.fail", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            return null;
        }
        AmazonS3DataSource dataSource = (AmazonS3DataSource) provider.getDataSource();
        return dataSource;
    }
    public boolean createPipelineByBucket(RequestContext ctx, AwsETFolderConfig etConfig) throws RepositoryException {
        ElasticTranscoderConfigService config = (ElasticTranscoderConfigService) SpringContextSingleton.getBean(ElasticTranscoderConfigService.BEAN_NAME);
        RenderContext rc = getRenderContext(ctx);
        AmazonS3DataSource dataSource = getDataSource(ctx,etConfig);
        if(null == dataSource){
            MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
            mb.defaultText(Messages.get(BUNDLE, "aws.et.pipeline.mp.fail", LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
            return false;
        }
        String bucket=dataSource.getBucket();
        String accessName=dataSource.getAccessKeyName();
        AwsAccess awsAccess=config.getAwsService().getAccessConfig(accessName);
        AmazonElasticTranscoderClient client = new AmazonElasticTranscoderClient(awsAccess.createBasicAWSCredentials());
        client.setRegion(RegionUtils.getRegion(awsAccess.getRegionName()));
        ListPipelinesResult pipelinesResult = client.listPipelines();
        MessageBuilder mb = new MessageBuilder().source(ET_SOURCE_TYPE).error();
        try {
            for (Pipeline pipeline : pipelinesResult.getPipelines()) {
                if(bucket.equals(pipeline.getInputBucket())&&bucket.equals(pipeline.getOutputBucket())){
                    etConfig.setPipelineId(pipeline.getId());
                    return true;
                }
            }
        } catch (AmazonServiceException e) {
            mb.defaultText(Messages.get(BUNDLE, e.getErrorMessage(), LocaleContextHolder.getLocale()));
            ctx.getMessageContext().addMessage(mb.build());
        }
        mb.defaultText(Messages.get(BUNDLE, "aws.et.pipeline.find.fail", LocaleContextHolder.getLocale()));
        ctx.getMessageContext().addMessage(mb.build());

        /*
        if(false) {
            //todo check role;
            CreatePipelineRequest pipelineRequest = new CreatePipelineRequest();
            pipelineRequest.setName("Jahia-autocreate-" + bucket);
            pipelineRequest.setInputBucket(bucket);
            pipelineRequest.setOutputBucket(bucket);
            pipelineRequest.setRole("Elastic_Transcoder_Default_Role");
            try {
                CreatePipelineResult pipelineResult = client.createPipeline(pipelineRequest);
                if (null != pipelineResult.getPipeline()) {
                    etConfig.setPipelineId(pipelineResult.getPipeline().getId());
                    return true;
                }
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        }
        */
        return false;
    }

    private RenderContext getRenderContext(RequestContext ctx) {
        return (RenderContext) ctx.getExternalContext().getRequestMap().get("renderContext");
    }

}
