/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.et;

import com.amazonaws.services.elastictranscoder.AmazonElasticTranscoderClient;
import com.amazonaws.services.elastictranscoder.model.*;
import org.jahia.api.Constants;
import org.jahia.modules.external.aws.providers.AmazonS3DataSource;
import org.jahia.modules.external.aws.Constant;
import org.jahia.modules.external.aws.event.S3Event;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.content.JCRSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by naf on 11.06.2015.
 */
public class ElasticTranscoderFolderService {
    private static final Logger log =
            LoggerFactory.getLogger(ElasticTranscoderFolderService.class);

    ElasticTranscoderConfigService configService;
    etS3EventListener listener = new etS3EventListener();
    private HashMap<AmazonS3DataSource, AmazonElasticTranscoderClient> clientPoll = new HashMap<>();

    public void init() {
        configService.getAwsService().addEventListener(listener);
    }

    public void destroy() {
        configService.getAwsService().removeEventHandler(listener);
        clientPoll.clear();
    }

    public void createElasticTranscoderJob(S3Event event) throws RepositoryException {
        if (!event.getPath().endsWith(AmazonS3DataSource.JCR_CONTENT_SUFFIX))
            return;
        if (!(event.getSource() instanceof AmazonS3DataSource))
            return;
        String mimeType = event.getParam(Constants.JCR_MIMETYPE);
        if (null == mimeType || !mimeType.toLowerCase().startsWith("video/"))
            return;
        AmazonS3DataSource dataSource = (AmazonS3DataSource) event.getSource();
        String jcrFilePath = event.getPath();
        jcrFilePath = jcrFilePath.substring(0, jcrFilePath.lastIndexOf("/"));
        AwsETFolderConfig folderConfig = configService.getConfigByFolder(jcrFilePath, JCRSessionFactory.getInstance().getCurrentSystemSession("default", null, null));
        if (null == folderConfig) {
            log.info("Not found configuration for folder " + jcrFilePath);
            return;
        }
        if (null == folderConfig.getPipelineId()) {
            log.info("Not found any pipeline in configuration for folder " + jcrFilePath);
            return;
        }
        String mountPoint = event.getParam(AmazonS3DataSource.PARAM_MOUNT_POINT_PATH);
        String s3FilePath = jcrFilePath.substring(mountPoint.length() + 1);

        List<CreateJobOutput> outputs = new ArrayList<>();
        List<AwsETFolderConfig.Preset> presets = folderConfig.getPresets();

        if (presets.size() == 0 || s3FilePath.indexOf("/") < 0)
            return;
        for (AwsETFolderConfig.Preset preset : presets) {
            outputs.add(new CreateJobOutput().withKey(preset.getKey()+"."+preset.getContainer()).withPresetId(preset.getId()));
        }

        JobInput input = new JobInput().withKey(s3FilePath);

        CreateJobRequest createJobRequest = new CreateJobRequest()
                .withPipelineId(folderConfig.getPipelineId())
                .withInput(input)
                .withOutputKeyPrefix(s3FilePath + "." + Constant.THUMBNAIL + "/")
                .withOutputs(outputs);
        AmazonElasticTranscoderClient client = clientPoll.get(dataSource);
        if (null == client) {
            AwsAccess config = dataSource.getAccessConfig();
            client = new AmazonElasticTranscoderClient(config.createBasicAWSCredentials());
            client.setRegion(config.getRegion());
            clientPoll.put(dataSource, client);
        }
        client.createJob(createJobRequest).getJob();
        log.info("Add " + Constant.THUMBNAIL + " for file s3 " + jcrFilePath);
    }

    public void setConfigService(ElasticTranscoderConfigService configService) {
        this.configService = configService;
    }

    public class etS3EventListener implements EventListener {

        @Override
        public void onEvent(EventIterator events) {
            while (events.hasNext()) {
                try {
                    S3Event event = (S3Event) events.nextEvent();
                    if ((Event.NODE_ADDED & event.getType()) != 0)
                        createElasticTranscoderJob(event);
                } catch (RepositoryException e) {
                    log.error("Error handele event for s3 video file  ", e);
                }
            }
        }

    }

}
