<fmt:message var="i18nRemoveConfirm" key="aws.delete.confirm"/><c:set var="i18nRemoveConfirm" value="${functions:escapeJavaScript(i18nRemoveConfirm)}"/>
<fmt:message var="i18nRemove" key="label.remove"/><c:set var="i18nRemove" value="${functions:escapeJavaScript(i18nRemove)}"/>
<fmt:message var="i18nEdit" key="label.edit"/><c:set var="i18nEdit" value="${functions:escapeJavaScript(i18nEdit)}"/>
<fmt:message var="i18nEditAWS" key="aws.editInAWSConsole"/><c:set var="i18nEditAWS" value="${functions:escapeJavaScript(i18nEditAWS)}"/>
<fmt:message var="i18nNameMandatory" key="aws.errors.name.mandatory"/><c:set var="i18nNameMandatory" value="${fn:escapeXml(i18nNameMandatory)}"/>
<fmt:message var="i18nAccessName" key="aws.errors.accessName"/><c:set var="i18nAccessName" value="${fn:escapeXml(i18nAccessName)}"/>

