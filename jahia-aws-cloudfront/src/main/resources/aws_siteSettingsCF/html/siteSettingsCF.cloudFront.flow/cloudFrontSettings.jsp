<%@include file="../siteSettingsCF.base.jsp" %>

<c:set var="isUpdate" value="${cf.update}"/>

<template:addResources>
    <script type="text/javascript">
        function fancyboxShowActivity(fieldId){
//            $.fancybox.showLoading();
            $("#"+fieldId+"-treeItemSelector").bind("add",function() {
                $.fancybox.hideLoading();
                $(".hasChildren.expandable").find( "span" ).bind("click",function() {$.fancybox.showLoading();});
                $(".hasChildren.expandable").find( "div" ).bind("click",function() {$.fancybox.showLoading();});
            });
        }
        function showErrors(messages, separator) {
            var message = "";
            for(var i = 0; i < messages.length; i++){
                if(i == 0){
                    message = messages[i];
                }else {
                    message += (separator + messages[i]);
                }
            }
            $("#cfFormErrorMessages").html(message);
            $("#cfFormErrorContainer").show();
        }

        function hideErrors() {
            $("#cfFormErrorMessages").empty();
            $("#cfFormErrorContainer").hide();
        }

        function submitCloudFrontForm(act) {
            $('#cloudFrontFormAction').val(act);
            $('#cloudFrontForm').submit();
        }

        $(document).ready(function() {
            $("#cfFormErrorClose").bind("click", function(){
                hideErrors();
            });
            $('#cloudFrontForm').submit(function () {
                if ($('#cloudFrontFormAction').val()=='cancel')
                    return true;
                // validate fields
                var messages = [];
                if (!$("#cloudFrontTitle").val().trim()) {
                    messages.push("${i18nNameMandatory}");
                }
                if (!$("#cloudFrontDistributionId").val() && messages.length < 1) {
                    messages.push("${i18nDistributionId}");
                }
                if (!$("#cloudFrontAccessName").val() && messages.length < 1) {
                    messages.push("${i18nAccessName}");
                }
                if (!$("#cloudFrontBasePath").val() && messages.length < 1) {
                    messages.push("${i18nBasePath}");
                }
                if (messages.length>0) {
                    showErrors(messages, "</br>");
                    return false;
                }
                return true;
            })
        });

    </script>
</template:addResources>

<div>
    <c:choose>
        <c:when test="${isUpdate}">
            <h2><fmt:message key="aws.cloudFront.edit"/> - ${fn:escapeXml(cf.title)}</h2>
        </c:when>
        <c:otherwise>
            <h2><fmt:message key="aws.cloudFront.add"/></h2>
        </c:otherwise>
    </c:choose>
    <div class="box-1">
        <div class="alert alert-error" style="display: none" id="cfFormErrorContainer">
            <button type="button" class="close" id="cfFormErrorClose">&times;</button>
            <span id="cfFormErrorMessages"></span>
        </div>
        <c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
            <c:if test="${message.severity eq 'INFO'}">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    INFO:&nbsp;${message.text}
                </div>
            </c:if>
            <c:if test="${message.severity eq 'ERROR'}">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    ERROR:&nbsp;${message.text}
                </div>
            </c:if>
            <c:if test="${message.severity eq 'WARNING'}">
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    WARNING:&nbsp;${message.text}
                </div>
            </c:if>
        </c:forEach>
        <form action="${flowExecutionUrl}" method="POST" id="cloudFrontForm">
            <input type="hidden" name="_eventId" id="cloudFrontFormAction" value="ok"/>
            <fieldset>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span6">
                            <form:checkbox path="cf.switchOn"/>&nbsp;<fmt:message key="aws.cloudFront.switchOn"/>
                        </div>
                        <div class="span6">
                            <label for="cloudFrontTitle"><fmt:message key="aws.cloudFront.name"/> <span  style="visibility: ${isUpdate ? 'hidden' : 'visible'}" class="text-error"><strong>*</strong></span></label>
                            <form:input path="cf.title" id="cloudFrontTitle" disabled="${isUpdate ? true : false}"/>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <label for="cloudFrontAccessName"><fmt:message key="aws.accsess.key"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:select path="cf.accessName" id="cloudFrontAccessName" items="${accessList}"/>
                        </div>
                        <div class="span6">
                            <label for="cloudFrontDistributionId"><fmt:message key="aws.cloudFront.distributionId"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input path="cf.distributionId" id="cloudFrontDistributionId" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <label for="cloudFrontBasePath"><fmt:message key="aws.cloudFront.basePath"/> <span  style="visibility: ${isUpdate ? 'hidden' : 'visible'}" class="text-error"><strong>*</strong></span></label>
                            <%--<form:select path="cf.basePath" id="cloudFrontMPointPath"  disabled="${isUpdate ? true : false}" items="${mountPointList}"/>--%>
                            <form:input path="cf.basePath" id="cloudFrontBasePath"  readonly="true"/>
                            <c:if test="${!isUpdate}">
                            <ui:folderSelector root="/sites/" fieldId="cloudFrontBasePath"
                                               includeChildren="false" displayIncludeChildren="false"
                                               valueType="path"
                                               selectableNodeTypes="jnt:folder,jnt:mounts"
                                               nodeTypes="jmix:accessControlled,jnt:folder,jnt:mounts"
                                               onSelect="function() {$.fancybox.hideLoading(); return true;}"
                                               onClose="function() {$.fancybox.hideLoading();}"
                                               fancyboxOptions="{onUpdate : function(){fancyboxShowActivity('cloudFrontBasePath');}}"
                            />
                            </c:if>
                        </div>
                        <div class="span6">
                            <label for="cloudFrontInvalidationPeriod"><fmt:message key="aws.cloudFront.invalidationPeriod"/></label>
                            <form:input path="cf.invalidationPeriod" id="cloudFrontInvalidationPeriod" />
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <button class="btn btn-primary" id="saveTemplateSettings" >
                                <i class="icon-${isUpdate ? 'share' : 'plus'} icon-white"></i>
                                &nbsp;<fmt:message key="label.${isUpdate ? 'update' : 'add'}"/>
                            </button>
                            <button class="btn" onclick="submitCloudFrontForm('cancel'); return false;">
                                <i class="icon-ban-circle"></i>
                                &nbsp;<fmt:message key="label.cancel"/>
                            </button>
                        </div>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>
</div>