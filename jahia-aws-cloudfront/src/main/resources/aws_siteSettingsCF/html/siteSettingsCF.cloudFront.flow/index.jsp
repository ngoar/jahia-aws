<%@include file="../siteSettingsCF.base.jsp" %>
<%--@elvariable id="cFront" type="org.jahia.services.content.JCRNodeWrapper"--%>

<template:addResources>
    <script type="text/javascript">
        function submitCloudFrontForm(act, uuid) {
            $('#cloudFrontFormAction').val(act);
            if (uuid) {
                $('#cloudFrontFormSelected').val(uuid);
            }
            $.fancybox.showLoading();
            $('#cloudFrontForm').submit();
        }
    </script>
</template:addResources>
<form action="${flowExecutionUrl}" method="post" style="display: inline;" id="cloudFrontForm">
    <input type="hidden" name="selectedCloudFront" id="cloudFrontFormSelected"/>
    <input type="hidden" name="_eventId" id="cloudFrontFormAction"/>
</form>

<h2><fmt:message key="aws.cloudFront.manage.info"/> <a href="https://console.aws.amazon.com/cloudfront/home?#distributions:" target="_blank">Management Console</a></h2>


<div class="box-1">

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th width="3%">#</th>
            <th><fmt:message key="label.name"/></th>
            <th><fmt:message key="aws.cloudFront.basePath"/></th>
            <th><fmt:message key="aws.cloudFront.domainName"/></th>
            <th><fmt:message key="aws.cloudFront.distributionId"/></th>
            <th><fmt:message key="aws.cloudFront.invalidationPeriod"/></th>
            <th><fmt:message key="aws.cloudFront.status"/></th>
            <th width="150"><fmt:message key="label.actions"/></th>
        </tr>
        </thead>
        <tbody>
        <c:set value="${jcr:getChildrenOfType(cFront,'aws:cloudFrontConfig')}" var="cfConfigs"/>
        <c:if test="${empty cfConfigs }">
            <tr>
                <td colspan="7"><fmt:message key="label.noItemFound"/></td>
            </tr>
        </c:if>
        <c:forEach items="${cfConfigs}" var="cfConf" varStatus="loopStatus">
            <tr>
                <td>${loopStatus.count}</td>
                <td><a title="<fmt:message key='label.edit'/>&nbsp;${cfConf.properties['jcr:title'].string}" href="#edit" onclick="submitCloudFrontForm('editCloudFrontSettings', '${cfConf.identifier}');return false;">${cfConf.properties['jcr:title'].string}</a></td>
                <td>${cfConf.properties['j:basePath'].string}</td>
                <td>${cfConf.properties['j:domainName'].string}</td>
                <td>${cfConf.properties['j:distributionId'].string}</td>
                <td>${cfConf.properties['j:invalidationPeriod'].long}</td>
                <td><a href="#switch"
                       title="<fmt:message key='aws.cloudFront.status.${cfConf.properties["j:switchOn"].boolean ? "on" : "off"}'/>"
                       onclick="submitCloudFrontForm('switch', '${cfConf.identifier}');return false;">
                        <img src="<c:url value="${url.currentModule}/icons/${cfConf.properties['j:switchOn'].boolean ? 'on-success' : 'off-error'}-icon.png"/>"
                             width="25" height="25">
                    </a></td>
                <td>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#edit"
                       onclick="submitCloudFrontForm('editCloudFrontSettings', '${cfConf.identifier}');return false;">
                        <i class="icon-pencil"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEditAWS}" href="https://console.aws.amazon.com/cloudfront/home?#distribution-settings:${cfConf.properties['j:distributionId'].string}" target="_blank">
                        <i class="icon-share-alt"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}"
                       href="#delete"
                       onclick="if (confirm('${i18nRemoveConfirm}')) {submitCloudFrontForm('removeCloudFrontSettings', '${cfConf.identifier}');} return false;">
                        <i class="icon-remove icon-white"></i>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <div class="buttons">
        <button id="addTemplate" type="button" class="btn btn-primary"
                onclick="submitCloudFrontForm('addCloudFrontSettings')">
            <i class="icon-plus icon-white"> </i><fmt:message key="aws.cloudFront.add"/></button>
        <div class="clear"></div>
    </div>
</div>