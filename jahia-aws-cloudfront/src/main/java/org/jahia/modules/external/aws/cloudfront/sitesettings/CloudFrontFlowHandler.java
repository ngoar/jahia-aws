/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.cloudfront.sitesettings;

import org.jahia.modules.external.aws.cloudfront.services.CloudFrontService;
import org.jahia.modules.external.aws.cloudfront.sitesettings.models.CloudFrontModel;
import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.decorator.JCRMountPointNode;
import org.jahia.services.render.RenderContext;
import org.jahia.utils.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.webflow.execution.RequestContext;

import javax.jcr.RepositoryException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexander Storozhuk on 15.06.2015.
 * Time: 13:10
 */
public class CloudFrontFlowHandler implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(CloudFrontFlowHandler.class);
    private static final String BUNDLE = "resources.jahia-aws-cloudfront";

    private static final String CF_SOURCE_TYPE = "aws_cloudFrontConfig";
    private static final long serialVersionUID = 2182171603384500580L;
    @Autowired
    private transient CloudFrontService cloudFrontService;
    @Autowired
    private transient AwsService awsService;

    public CloudFrontModel createModel(RequestContext ctx, String identifier) {
        if (identifier != null && !identifier.trim().isEmpty()) {
            try {
                JCRSessionWrapper session = getCurrentUserSession(ctx);
                JCRNodeWrapper node = session.getNodeByIdentifier(identifier);
                return new CloudFrontModel(node);
            } catch (RepositoryException e) {
                logger.warn("Cant find CloudFront by identifier " + identifier, e);
            }
        }
        return new CloudFrontModel();
    }


    public boolean addCloudFrontConfiguration(RequestContext ctx, CloudFrontModel cfm) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);

            for (String mountPointPath : getMountPointsPath(ctx)) {
                if(mountPointPath.equals(cfm.getBasePath())) {
                    cloudFrontService.createUpdateCloudFrontConfigNode(session, cfm);
                    return true;
                }

                if(cfm.getBasePath().startsWith(mountPointPath + "/")){
                    MessageBuilder mb = new MessageBuilder().source(CF_SOURCE_TYPE).warning();
                    mb.defaultText(Messages.get(BUNDLE, "aws.cloudFront.errors.basePath.s3found", LocaleContextHolder.getLocale()) + " " + mountPointPath);
                    ctx.getMessageContext().addMessage(mb.build());
                    cfm.setBasePath(mountPointPath);
                    return false;
                }
            }
            cloudFrontService.createUpdateCloudFrontConfigNode(session, cfm);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source(CF_SOURCE_TYPE);
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    public boolean removeCloudFrontConfiguration(RequestContext ctx, String identifier) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper node = getNodeByUUID(identifier, session);
            cloudFrontService.deleteCloudFrontConfigNode(node);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source(CF_SOURCE_TYPE);
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    public boolean switchStatusCloudFrontConfiguration(RequestContext ctx, String identifier) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper node = getNodeByUUID(identifier, session);
            cloudFrontService.switchCloudFrontConfigNode(node);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source(CF_SOURCE_TYPE);
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    public List<String> getMountPointsPath(RequestContext ctx) throws RepositoryException {
        List<JCRMountPointNode> mountpoints = awsService.getMountpointsNode(getCurrentUserSession(ctx));
        List<String> mPointPathsList = new ArrayList<>(mountpoints.size());
        for (JCRMountPointNode mPoint : mountpoints) {
            mPointPathsList.add(mPoint.getTargetMountPointPath());
        }
        return mPointPathsList;
    }

    public List<String> getAccessList(RequestContext ctx) throws RepositoryException {
        Map<String, AwsAccess> accessMap = awsService.getAccessConfigs(getCurrentUserSession(ctx));
        List<String> accessList = new ArrayList<>(accessMap.size());
        for (String accessName : accessMap.keySet()) {
            accessList.add(accessName);
        }
        return accessList;
    }

    protected JCRNodeWrapper getNodeByUUID(String identifier, JCRSessionWrapper session) {
        try {
            return session.getNodeByUUID(identifier);
        } catch (RepositoryException e) {
            logger.error("Error retrieving node with UUID " + identifier, e);
        }
        return null;
    }

    protected JCRSessionWrapper getCurrentUserSession(RequestContext ctx) throws RepositoryException {
        RenderContext renderContext = getRenderContext(ctx);
        return renderContext.getMainResource().getNode().getSession();
    }

    protected RenderContext getRenderContext(RequestContext ctx) {
        return (RenderContext) ctx.getExternalContext().getRequestMap().get("renderContext");
    }

    public JCRNodeWrapper getCloudFrontConfigNode(RequestContext ctx) throws RepositoryException {
        JCRSessionWrapper session = getCurrentUserSession(ctx);
        return cloudFrontService.getCloudFrontRootNode(session);
    }
}
