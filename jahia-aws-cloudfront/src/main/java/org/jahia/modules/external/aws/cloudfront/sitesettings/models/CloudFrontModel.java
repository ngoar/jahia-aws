/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.cloudfront.sitesettings.models;

import org.jahia.modules.external.aws.Constant;
import org.jahia.modules.external.aws.cloudfront.services.CloudFrontConfiguration;
import org.jahia.services.content.JCRNodeWrapper;

import javax.jcr.RepositoryException;
import java.io.Serializable;

/**
 * Created by Alexander Storozhuk on 15.06.2015.
 * Time: 13:34
 */
public class CloudFrontModel implements Serializable {
    String cloudFrontUUID;
    String title;
    boolean switchOn;
    String distributionId;
    String basePath;
    int invalidationPeriod;
    String accessName;
    boolean isUpdate = false;

    public CloudFrontModel(JCRNodeWrapper node) throws RepositoryException {
        setCloudFrontUUID(node.getIdentifier());
        setTitle(node.getPropertyAsString(CloudFrontConfiguration.TITLE_FIELD));
        setSwitchOn(node.getProperty(CloudFrontConfiguration.SWITCH_ON_FIELD).getBoolean());
        setDistributionId(node.getPropertyAsString(CloudFrontConfiguration.DISTRIBUTION_ID_FIELD));
        setInvalidationPeriod((int) node.getProperty(CloudFrontConfiguration.INVALIDATION_PERIOD_FIELD).getLong());
        setBasePath(node.getPropertyAsString(CloudFrontConfiguration.BASE_PATH_FIELD));
        setAccessName(node.getPropertyAsString(Constant.ACCESS_FIELD));
        setUpdate(true);
    }

    public CloudFrontModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSwitchOn() {
        return switchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        this.switchOn = switchOn;
    }

    public String getDistributionId() {
        return distributionId;
    }

    public void setDistributionId(String distributionId) {
        this.distributionId = distributionId;
    }

    public int getInvalidationPeriod() {
        return invalidationPeriod;
    }

    public void setInvalidationPeriod(int invalidationPeriod) {
        this.invalidationPeriod = invalidationPeriod;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getCloudFrontUUID() {
        return cloudFrontUUID;
    }

    public void setCloudFrontUUID(String cloudFrontUUID) {
        this.cloudFrontUUID = cloudFrontUUID;
    }

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }
}
