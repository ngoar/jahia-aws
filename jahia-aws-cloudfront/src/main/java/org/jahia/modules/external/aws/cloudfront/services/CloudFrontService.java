/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.cloudfront.services;

import org.jahia.modules.durlrewrite.DynamicUrlRewriteService;
import org.jahia.modules.external.aws.Constant;
import org.jahia.modules.external.aws.cloudfront.rules.CloudFrontOutboundRule;
import org.jahia.modules.external.aws.cloudfront.sitesettings.models.CloudFrontModel;
import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.services.content.JCRContentUtils;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.rules.BackgroundAction;
import org.jahia.services.query.QueryWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import java.util.Hashtable;

/**
 * Created by Alexander Storozhuk on 08.06.2015.
 * Time: 17:29
 */
public class CloudFrontService implements BackgroundAction {

    public static final String BEAN_NAME = "awsCloudFrontService";
    public static final String C_FRONT_CONFIG_LIST_NODE_TYPE = "aws:cloudFrontList";

    private static final Logger logger = LoggerFactory.getLogger(CloudFrontService.class);
    private Hashtable<String, CloudFrontConfiguration> cfConfigsMap;
    private AwsService awsService;
    private DynamicUrlRewriteService urlRewriteService;
    private CloudFrontOutboundRule outboundRule;

    public void init() throws RepositoryException {
        try {
            cfConfigsMap = new Hashtable<>();
            JCRNodeWrapper configNodeList = getCloudFrontRootNode(JCRSessionFactory.getInstance().getCurrentSystemSession("default", null, null));
            for (JCRNodeWrapper cfNode : configNodeList.getNodes()) {
                CloudFrontConfiguration cloudFront = new CloudFrontConfiguration(cfNode, awsService);
                cfConfigsMap.put(cloudFront.getBasePath(), cloudFront);
                awsService.addEventListener(cloudFront);
            }
            outboundRule = new CloudFrontOutboundRule(this);
            urlRewriteService.addOutboundRule(outboundRule, DynamicUrlRewriteService.SCOPE_BASE);

        } catch (Exception ex) {
            logger.error("Problem to load CloudFront Service configurations", ex.getMessage(), ex);
        }
    }

    public void destroy() {
        for (String key : cfConfigsMap.keySet()) {
            awsService.removeEventHandler(cfConfigsMap.get(key));
            urlRewriteService.removeOutboundRule(outboundRule, DynamicUrlRewriteService.SCOPE_BASE);
        }
        cfConfigsMap.clear();
    }

    /**
     * Search CloudFront root node in JCR
     * @param session JCR Session
     * @return JCR CloudFront root node
     * @throws RepositoryException
     */
    public JCRNodeWrapper getCloudFrontRootNode(JCRSessionWrapper session) throws RepositoryException {
        QueryWrapper query = session.getWorkspace().getQueryManager().createQuery("SELECT * from [" + C_FRONT_CONFIG_LIST_NODE_TYPE + "]", Query.JCR_SQL2);
        for (JCRNodeWrapper cfNodeConfig : query.execute().getNodes()) {
            return cfNodeConfig;
        }
        throw new RepositoryException("Can't find CloudFront root 'list' node!");
    }

    public CloudFrontConfiguration getCloudFrontConfigurationMatch(String filePath) {
        for (String mpPath : cfConfigsMap.keySet()) {
            if (filePath.startsWith(mpPath + "/") && getCloudFrontConfiguration(mpPath).isSwitchOn())
                return getCloudFrontConfiguration(mpPath);
        }
        return null;
    }


    public CloudFrontConfiguration getCloudFrontConfiguration(String mPointPath) {
        return cfConfigsMap.get(mPointPath);
    }

    /**
     * Delete CloudFront configuration node & local config obj mapping
     *
     * @param cloudFrontConfigNode CloudFront config
     * @throws RepositoryException
     */
    public void deleteCloudFrontConfigNode(JCRNodeWrapper cloudFrontConfigNode) throws RepositoryException {
        JCRSessionWrapper session = cloudFrontConfigNode.getSession();
        String basePath = cloudFrontConfigNode.getPropertyAsString(CloudFrontConfiguration.BASE_PATH_FIELD);
        if (null != cfConfigsMap)
            awsService.removeEventHandler(cfConfigsMap.remove(basePath));
        cloudFrontConfigNode.remove();
        session.save();
    }

    /**
     * Create or update configuration node & local config obj mapping
     *
     * @param session
     * @param model
     */
    public void createUpdateCloudFrontConfigNode(JCRSessionWrapper session, CloudFrontModel model) throws RepositoryException {
        JCRNodeWrapper newCfConfigNode;
        if (model.isUpdate()) {
            newCfConfigNode = session.getNodeByUUID(model.getCloudFrontUUID());
            session.checkout(newCfConfigNode);
        } else {
            JCRNodeWrapper cfConfigNodeList = getCloudFrontRootNode(session);
            newCfConfigNode = cfConfigNodeList.addNode(JCRContentUtils.findAvailableNodeName(cfConfigNodeList, model.getTitle()), CloudFrontConfiguration.C_FRONT_NODE_TYPE);
            newCfConfigNode.setProperty(CloudFrontConfiguration.TITLE_FIELD, model.getTitle());
            newCfConfigNode.setProperty(CloudFrontConfiguration.BASE_PATH_FIELD, model.getBasePath());
        }
        newCfConfigNode.setProperty(CloudFrontConfiguration.SWITCH_ON_FIELD, model.isSwitchOn());
        newCfConfigNode.setProperty(CloudFrontConfiguration.DISTRIBUTION_ID_FIELD, model.getDistributionId());
        newCfConfigNode.setProperty(CloudFrontConfiguration.INVALIDATION_PERIOD_FIELD, model.getInvalidationPeriod());
        newCfConfigNode.setProperty(CloudFrontConfiguration.INVALIDATION_PERIOD_FIELD, model.getInvalidationPeriod());
        newCfConfigNode.setProperty(Constant.ACCESS_FIELD, model.getAccessName());
        if (null == cfConfigsMap)
            cfConfigsMap = new Hashtable<>();
        CloudFrontConfiguration cfConfig = new CloudFrontConfiguration(newCfConfigNode, awsService);
        cfConfigsMap.put(model.getBasePath(), cfConfig);
        awsService.addEventListener(cfConfig);
        newCfConfigNode.setProperty(CloudFrontConfiguration.DOMAIN_NAME_FIELD, cfConfig.getDomainName());
        session.save();
    }

    /**
     * Switch on or off CloudFront config
     *
     * @param cloudFrontConfigNode CloudFront JCR config node
     * @throws RepositoryException
     */
    public void switchCloudFrontConfigNode(JCRNodeWrapper cloudFrontConfigNode) throws RepositoryException {
        JCRSessionWrapper session = cloudFrontConfigNode.getSession();
        String basePath = cloudFrontConfigNode.getPropertyAsString(CloudFrontConfiguration.BASE_PATH_FIELD);
        CloudFrontConfiguration cloudFrontConfig = cfConfigsMap.get(basePath);
        boolean status = !cloudFrontConfig.isSwitchOn();
        cloudFrontConfig.setSwitchOn(status);
        cloudFrontConfigNode.setProperty(CloudFrontConfiguration.SWITCH_ON_FIELD, status);
        session.save();
    }

    public AwsService getAwsService() {
        return awsService;
    }

    public void setAwsService(AwsService awsService) {
        this.awsService = awsService;
    }

    public void setUrlRewriteService(DynamicUrlRewriteService urlRewriteService) {
        this.urlRewriteService = urlRewriteService;
    }

    public DynamicUrlRewriteService getUrlRewriteService() {
        return urlRewriteService;
    }

    @Override
    public String getName() {
        return BEAN_NAME;
    }

    @Override
    public void executeBackgroundAction(JCRNodeWrapper node) {
        CloudFrontConfiguration config = getCloudFrontConfigurationMatch(node.getPath());
        if (null == config)
            return;
        config.createInvalidation(node.getPath());
    }
}
