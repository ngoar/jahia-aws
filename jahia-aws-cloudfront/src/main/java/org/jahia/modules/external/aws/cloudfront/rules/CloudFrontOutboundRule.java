/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.cloudfront.rules;

import org.apache.commons.lang.StringUtils;
import org.jahia.api.Constants;
import org.jahia.modules.external.aws.cloudfront.services.CloudFrontConfiguration;
import org.jahia.modules.external.aws.cloudfront.services.CloudFrontService;
import org.jahia.services.content.JCRContentUtils;
import org.jahia.services.render.filter.ContextPlaceholdersReplacer;
import org.jahia.settings.SettingsBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tuckey.web.filters.urlrewrite.OutboundRule;
import org.tuckey.web.filters.urlrewrite.RewrittenOutboundUrl;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URLDecoder;

/**
 * Created by Alexander Storozhuk on 06.07.2015.
 * Time: 17:11
 */
public class CloudFrontOutboundRule extends OutboundRule {
    private static final Logger logger = LoggerFactory.getLogger(CloudFrontOutboundRule.class);
    public static final String DEFAULT_NAME = "CloudFront Outbound rule";
    private String characterEncoding;
    CloudFrontService service;

    public CloudFrontOutboundRule(CloudFrontService service) {
        this.service = service;
    }

    @Override
    public String getDisplayName() {
        return DEFAULT_NAME;
    }

    @Override
    public boolean initialise(ServletContext servletContext) {
        characterEncoding = SettingsBean.getInstance().getCharacterEncoding();
        return true;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public String getMatchType() {
        return super.getMatchType();
    }

    @Override
    public RewrittenOutboundUrl execute(String url, HttpServletRequest hsRequest, HttpServletResponse hsResponse)
            throws InvocationTargetException {
        String filePath = null;
        try {
            filePath = parsePath(url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (filePath == null || !StringUtils.isNotEmpty(filePath)) {
            return null;
        }
        CloudFrontConfiguration config = service.getCloudFrontConfigurationMatch(filePath);
        if (null == config)
            return null;
        String rewriteUrl = "";
        try {
            rewriteUrl = config.getPublicUrl(config.isS3mountPoint()?config.getRelevantPath(filePath):url, false).toString();
        } catch (MalformedURLException e) {
            logger.error("Problem to execute " + DEFAULT_NAME, e, e.getMessage());
            return null;
        }
        return new RewrittenOutboundUrl(rewriteUrl, hsRequest.isSecure());
    }

    protected String parsePath(String url) throws UnsupportedEncodingException {
        String workspace = null;
        String path = null;
        String p = url.startsWith("/files/") ? url.substring("/files".length()) : url;
        if (p != null && p.length() > 2) {
            int pathStart = p.indexOf("/", 1);
            workspace = pathStart > 1 ? p.substring(1, pathStart) : null;
            if (workspace != null) {
                path = p.substring(pathStart);
                if (Constants.EDIT_WORKSPACE.equals(workspace)) {
                    // do not rewrite default workspace
                    workspace = null;
                }
                if (!JCRContentUtils.isValidWorkspace(workspace)) {
                    // unknown workspace
                    workspace = null;
                }
            }
        }

        return path != null && workspace != null ? JCRContentUtils.escapeNodePath(path) : null;
    }

}
