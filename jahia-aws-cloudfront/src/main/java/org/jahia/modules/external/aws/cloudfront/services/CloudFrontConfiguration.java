/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.cloudfront.services;

import com.amazonaws.Protocol;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.CloudFrontUrlSigner;
import com.amazonaws.services.cloudfront.model.*;
import org.jahia.modules.external.aws.Constant;
import org.jahia.modules.external.aws.event.S3Event;
import org.jahia.modules.external.aws.providers.AmazonS3DataSource;
import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.content.JCRNodeWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

/**
 * Created by Alexander Storozhuk on 19.06.2015.
 * Time: 18:19
 */
public class CloudFrontConfiguration  implements EventListener {

    public static final String C_FRONT_NODE_TYPE = "aws:cloudFrontConfig";
    public static final String TITLE_FIELD = "jcr:title";
    public static final String SWITCH_ON_FIELD = "j:switchOn";
    public static final String INVALIDATION_PERIOD_FIELD = "j:invalidationPeriod";
    public static final String DISTRIBUTION_ID_FIELD = "j:distributionId";
    public static final String DOMAIN_NAME_FIELD = "j:domainName";
    public static final String BASE_PATH_FIELD = "j:basePath";

    private static final Logger logger = LoggerFactory.getLogger(CloudFrontConfiguration.class);

    private AwsService awsService;

    /* By default it is switched off */
    private boolean switchOn = false;
    private AmazonCloudFrontClient cf;
    private Distribution distribution;
    private List<String> aliases;
    private Set<String> invalidationItemsSet;
    /* Invalidation period in minutes */
    private int invalidationPeriod = 15;
    private Calendar lastInvalidationTime;
    private String basePath;
    private boolean s3mountPoint=false;

    private String rsaKeyPathFile;
    private String pkKeyPathFile;
    private String keyPairId;


    public CloudFrontConfiguration(JCRNodeWrapper cfConfigNode, AwsService awsService) throws RepositoryException {
        setAwsService(awsService);
        initFields(cfConfigNode);
    }

    private void initFields(JCRNodeWrapper cfConfig) throws RepositoryException {
        AwsAccess awsAccess=null;
        awsAccess = awsService.getAccessConfig(cfConfig.getPropertyAsString(Constant.ACCESS_FIELD));
        generateCf(awsAccess);
        setDistribution(cfConfig.getPropertyAsString(DISTRIBUTION_ID_FIELD));
        setInvalidationPeriod((int) cfConfig.getProperty(INVALIDATION_PERIOD_FIELD).getLong());
        setBasePath(cfConfig.getPropertyAsString(BASE_PATH_FIELD));
        for (String mp : awsService.getMountPointsPathList(cfConfig.getSession())) {
            if(mp.equals(getBasePath())){
                s3mountPoint=true;
                break;
            }
        }

        setSwitchOn(cfConfig.getProperty(SWITCH_ON_FIELD).getBoolean());

        //todo Path to private key
        /*         keyPairId = cfConfig.getPropertyAsString(Constant.ACCESS_KEY_FIELD);
        setPkKeyPathFile(cfConfig.getPropertyAsString("TO-DO"));
        setRsaKeyPathFile(cfConfig.getPropertyAsString("TO-DO"));*/
    }

    /**
     * Create distribution
     *
     * @param comment
     * @return created distribution
     */
    public Distribution createDistribution(String comment) {
        try {
            DistributionConfig distributionConfig = new DistributionConfig(System.currentTimeMillis() + "", true);

            Origins origins = new Origins();
            Origin origin = new Origin();
            origin.setDomainName(distribution.getDomainName());

            distributionConfig.setOrigins(origins);
            distributionConfig.setAliases(new Aliases().withItems(aliases));
            distributionConfig.setComment(comment);
            CreateDistributionResult distributionResult = cf.createDistribution(new CreateDistributionRequest(distributionConfig));
            distribution = distributionResult.getDistribution();
            return distribution;
        } catch (Exception ex) {
            logger.error("Problem with create distribution", ex.getMessage());
            return null;
        }
    }

    /**
     * Get distribution by id
     *
     * @param id distribution id
     * @return
     */
    public Distribution getDistribution(String id) {
        GetDistributionResult distributionResult = cf.getDistribution(new GetDistributionRequest(id));
        return distributionResult.getDistribution();
    }

    /**
     * Create Invalidations
     *
     * @param items The paths of the objects to invalidate.
     *              The path is relative to the distribution and must begin with a slash (/).
     *              You must enclose each invalidation object with the Path element tags.
     * @return invalidation
     */
    public Invalidation createInvalidations(Collection<String> items) {
        if (switchOn) {
            addInvalidationItems(items);
            if (isTimeToInvalidate())
                try {
                    Paths paths = new Paths();
                    paths.withItems(invalidationItemsSet);
                    paths.setQuantity(invalidationItemsSet.size());
                    InvalidationBatch invalidationBatch = new InvalidationBatch(paths, System.currentTimeMillis() + "");
                    CreateInvalidationRequest invalidationRequest = new CreateInvalidationRequest(distribution.getId(), invalidationBatch);
                    CreateInvalidationResult invalidationResult = cf.createInvalidation(invalidationRequest);
                    invalidationItemsSet.clear();
                    return invalidationResult.getInvalidation();
                } catch (Exception ex) {
                    logger.error("Problem to invalidate items: " + items, ex);
                }
        } else {
            logger.info("CloudFront service is switched  off");
        }
        return null;
    }

    /**
     * Create Invalidation
     *
     * @param event Contains the path of the object to invalidate.
     *              The path is relative to the distribution and must begin with a slash (/).
     *              You must enclose each invalidation object with the Path element tags.
     * @return invalidation
     */

    public Invalidation createInvalidation(S3Event event) {
        if (!basePath.equals(event.getParam(AmazonS3DataSource.PARAM_MOUNT_POINT_PATH)))
            return null;
        String item = event.getParam(AmazonS3DataSource.PARAM_JCR_PATH);
        return createInvalidation(item);
    }

    public Invalidation createInvalidation(String item) {
        item = item.startsWith("/") ? item : ("/" + item);
        Set<String> set = new HashSet<>();
        set.add(item);
        return createInvalidations(set);
    }

        /**
         * Generate url for object in cash CloudFront
         *
         * @param path   The path of the object.
         * @param secure type of the protocol to use.
         * @return URL, that's will be available just if this object publishing in Amazon S3 bucket.
         * A public URL for an object in an Amazon S3 bucket uses this format:
         * http://<CloudFront domain name>/<object name in Amazon S3 bucket>
         * @throws MalformedURLException
         */
    public URL getPublicUrl(String path, boolean secure) throws MalformedURLException {
        URL url = null;
        if (switchOn) {
            path = path.startsWith("/") ? path : ("/" + path);
            url = new URL(secure ? Protocol.HTTPS.toString() : Protocol.HTTP.toString(), distribution.getDomainName(), path);
        } else {
            logger.info("CloudFront service is switched  off");
        }
        return url;
    }

    /**
     * Returns a signed URL with a canned policy that grants universal access to
     * private content until a given date.
     *
     * @param path       The s3 path of the object
     * @param secure     type of the protocol of the URL
     * @param expiration The expiration date of the signed URL in UTC
     * @return The signed URL string.
     * @throws InvalidKeySpecException
     * @throws IOException
     */
    //ToDo
    public String getLimitedUrl(String path, boolean secure, Date expiration) throws InvalidKeySpecException, IOException {
        if (null == pkKeyPathFile || pkKeyPathFile.isEmpty() || null == keyPairId)
            return null;
        path = path.startsWith("/") ? path.substring(1) : path;
        File rsaKeyFile = new File(pkKeyPathFile);
        return CloudFrontUrlSigner.getSignedURLWithCannedPolicy(secure ? CloudFrontUrlSigner.Protocol.https : CloudFrontUrlSigner.Protocol.http, distribution.getDomainName(), rsaKeyFile, path, keyPairId, expiration);
    }


    private void addInvalidationItems(Collection<String> items) {
        for (String inPath : items) {
            if (null == invalidationItemsSet) {
                invalidationItemsSet = new HashSet<>();
                invalidationItemsSet.add(inPath);
            } else {
                if (inPath.endsWith("/")) {
                    Set<String> pathsToDelete = new HashSet<>();
                    for (String currPath : invalidationItemsSet) {
                        if (currPath.startsWith(inPath.substring(0, inPath.length() - 1))) {
                            pathsToDelete.add(currPath);
                        }
                    }
                    invalidationItemsSet.removeAll(pathsToDelete);
                    inPath = inPath.concat("*");
                }
                invalidationItemsSet.add(inPath);
            }
        }
    }

    private boolean isTimeToInvalidate() {
        Calendar currentTime = Calendar.getInstance();
        if (null == lastInvalidationTime) {
            setLastInvalidationTime(currentTime);
            return true;
        }
        Calendar cInvalidationTime = (Calendar) lastInvalidationTime.clone();
        cInvalidationTime.add(Calendar.MINUTE, invalidationPeriod);
        if (cInvalidationTime.getTimeInMillis() > currentTime.getTimeInMillis())
            return false;
        setLastInvalidationTime(currentTime);
        return true;
    }


    public void setAwsService(AwsService awsService) {
        if (null != awsService) {
            this.awsService = awsService;
        }
    }

    @Override
    public void onEvent(EventIterator events) {
        while (events.hasNext()) {
            S3Event event = (S3Event) events.nextEvent();
            if (event.getType() == Event.NODE_ADDED ||
                    event.getType() == Event.NODE_REMOVED ||
                    event.getType() == Event.NODE_MOVED)
                createInvalidation(event);
        }

    }

    public String getRelevantPath(String absolutePath){
        return absolutePath.startsWith(basePath +"/")?absolutePath.substring(basePath.length()+1):"";

    }
    public AmazonCloudFrontClient getCf() {
        return cf;
    }

    public void generateCf(AwsAccess awsAccess) {
        this.cf = new AmazonCloudFrontClient(new BasicAWSCredentials(awsAccess.getAccessKey(), awsAccess.getSecretKey()));
    }

    public Distribution getDistribution() {
        return distribution;
    }

    public void setDistribution(String distributionId) {
        GetDistributionResult distributionResult;
        if (null != cf && null != distributionId) {
            distributionResult = cf.getDistribution(new GetDistributionRequest(distributionId));
            distribution = distributionResult.getDistribution();
        }
    }

    public String getDomainName() {
        return distribution.getDomainName();
    }


    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }



    public Set<String> getInvalidationItemsSet() {
        return invalidationItemsSet;
    }

    public int getInvalidationPeriod() {
        return invalidationPeriod;
    }

    /**
     * Setting period for invalidation
     *
     * @param invalidationPeriod period in minutes
     */
    public void setInvalidationPeriod(int invalidationPeriod) {
        this.invalidationPeriod = invalidationPeriod;
    }

    public Calendar getLastInvalidationTime() {
        return lastInvalidationTime;
    }

    private void setLastInvalidationTime(Calendar lastInvalidationTime) {
        this.lastInvalidationTime = lastInvalidationTime;
    }

    public boolean isSwitchOn() {
        return switchOn;
    }



    /**
     * Switch on or off CloudFront service
     *
     * @param switchOn boolean value
     */
    public void setSwitchOn(boolean switchOn) {
        this.switchOn = switchOn;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }


    public String getRsaKeyPathFile() {
        return rsaKeyPathFile;
    }

    public void setRsaKeyPathFile(String rsaKeyPathFile) {
        this.rsaKeyPathFile = rsaKeyPathFile;
    }

    public String getPkKeyPathFile() {
        return pkKeyPathFile;
    }

    public void setPkKeyPathFile(String pkKeyPathFile) {
        this.pkKeyPathFile = pkKeyPathFile;
    }

    public boolean isS3mountPoint() {
        return s3mountPoint;
    }

    public void setS3mountPoint(boolean s3mountPoint) {
        this.s3mountPoint = s3mountPoint;
    }
}
