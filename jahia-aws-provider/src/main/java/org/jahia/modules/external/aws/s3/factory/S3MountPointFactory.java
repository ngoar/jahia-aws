/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.s3.factory;

import org.hibernate.validator.constraints.NotEmpty;
import org.jahia.modules.external.admin.mount.AbstractMountPointFactory;
import org.jahia.modules.external.admin.mount.validator.LocalJCRFolder;
import org.jahia.modules.external.aws.Constant;
import org.jahia.services.content.JCRNodeWrapper;

import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by naf on 07.07.2015.
 */
public class S3MountPointFactory extends AbstractMountPointFactory implements Serializable {
    private static final long serialVersionUID = 3348410678046793885L;

    @NotEmpty
    private String name;
    @LocalJCRFolder
    private String localPath;
    @NotEmpty
    private String access;
    @NotEmpty
    private String bucket;
    private String prefix;
    private boolean _public;
    private boolean direct;
    private Collection<String> accessList;
    private String jsonBukets;
    private Collection<String> defaultBukets;


    @Override
    public void populate(JCRNodeWrapper nodeWrapper) throws RepositoryException {
        super.populate(nodeWrapper);
        this.name = getName(nodeWrapper.getName());
        try {
            this.localPath = nodeWrapper.getProperty("mountPoint").getNode().getPath();
        } catch (PathNotFoundException e) {
            // no local path defined for this mount point
        }
        this.access = nodeWrapper.getPropertyAsString(Constant.ACCESS_FIELD);
        this.bucket = nodeWrapper.getPropertyAsString(Constant.BUCKET_FIELD);
        this.prefix = nodeWrapper.getPropertyAsString(Constant.PREFIX_FIELD);
        this._public = nodeWrapper.getProperty(Constant.PUBLIC_FIELD).getBoolean();
        this.direct = nodeWrapper.getProperty(Constant.DIRECT_FIELD).getBoolean();
        defaultBukets = new ArrayList<>();
    }

    private void populateNode(JCRNodeWrapper nodeWrapper) throws RepositoryException {
        nodeWrapper.setProperty(Constant.ACCESS_FIELD, getAccess());
        nodeWrapper.setProperty(Constant.BUCKET_FIELD, getBucket());
        nodeWrapper.setProperty(Constant.PREFIX_FIELD, getPrefix());
        nodeWrapper.setProperty(Constant.PUBLIC_FIELD, isPublic());
        nodeWrapper.setProperty(Constant.DIRECT_FIELD, isDirect());
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    @Override
    public String getMountNodeType() {
        return Constant.S3_MP_NODE_TYPE;
    }

    @Override
    public void setProperties(JCRNodeWrapper jcrNodeWrapper) throws RepositoryException {
        populateNode(jcrNodeWrapper);

    }

    public String getJsonBukets() {
        return jsonBukets;
    }

    public void setJsonBuckets(String jsonBukets) {
        this.jsonBukets = jsonBukets;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = null != prefix ? prefix : "";
        if (this.prefix.startsWith("/"))
            this.prefix = this.prefix.substring(1);
        if (!this.prefix.endsWith("/") && this.prefix.length() > 0)
            this.prefix += "/";
    }

    public boolean isPublic() {
        return _public;
    }

    public void setPublic(boolean _public) {
        this._public = _public;
    }

    public Collection<String> getAccessList() {
        return accessList;
    }

    public void setAccessList(List<String> accessList) {
        this.accessList = accessList;
    }

    public boolean isDirect() {
        return direct;
    }

    public void setDirect(boolean direct) {
        this.direct = direct;
    }

    public Collection<String> getDefaultBukets() {
        return defaultBukets;
    }

    public void setDefaultBukets(Collection<String> defaultBukets) {
        this.defaultBukets = defaultBukets;
    }
}
