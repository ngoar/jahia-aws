/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 * <p>
 * http://www.jahia.com
 * <p>
 * Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 * <p>
 * THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 * 1/GPL OR 2/JSEL
 * <p>
 * 1/ GPL
 * ==================================================================================
 * <p>
 * IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * <p>
 * 2/ JSEL - Commercial and Supported Versions of the program
 * ===================================================================================
 * <p>
 * IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 * <p>
 * Alternatively, commercial and supported versions of the program - also known as
 * Enterprise Distributions - must be used in accordance with the terms and conditions
 * contained in a separate written agreement between you and Jahia Solutions Group SA.
 * <p>
 * If you are unsure which license is appropriate for your use,
 * please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.external.aws.s3;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import org.jahia.modules.external.admin.mount.AbstractMountPointFactoryHandler;
import org.jahia.modules.external.aws.s3.factory.S3MountPointFactory;
import org.jahia.modules.external.aws.services.AwsService;
import org.jahia.modules.external.aws.services.impl.AwsServiceImpl;
import org.jahia.modules.external.aws.sitesettings.models.AwsAccess;
import org.jahia.services.SpringContextSingleton;
import org.jahia.services.content.*;
import org.jahia.services.render.RenderContext;
import org.jahia.utils.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.webflow.execution.RequestContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.jcr.RepositoryException;
import java.io.Serializable;
import java.util.*;

/**
 * Created by naf on 06.07.2015.
 */
public class S3MountPointFactoryHandler extends AbstractMountPointFactoryHandler<S3MountPointFactory> implements Serializable {
    private static final long serialVersionUID = 634999190883576309L;
    private static final Logger logger = LoggerFactory.getLogger(S3MountPointFactoryHandler.class);
    private static final String BUNDLE = "resources.jahia-aws-provider";


    private String stateCode;
    private String messageKey;
    private S3MountPointFactory s3MountPointFactory;

    public void init(RequestContext requestContext) {
        try {
            s3MountPointFactory = getNewMountPoint(requestContext);
            super.init(requestContext, s3MountPointFactory);
        } catch (RepositoryException e) {
            logger.error("Error retrieving mount point", e);
        }
        requestContext.getFlowScope().put("s3Factory", s3MountPointFactory);
    }

    public String getFolderList() {
        JSONObject result = new JSONObject();
        try {
            JSONArray folders = JCRTemplate.getInstance().doExecuteWithSystemSession(new JCRCallback<JSONArray>() {
                @Override
                public JSONArray doInJCR(JCRSessionWrapper session) throws RepositoryException {
                    return getSiteFolders(session.getWorkspace());
                }
            });

            result.put("folders", folders);
        } catch (RepositoryException e) {
            logger.error("Error trying to retrieve local folders", e);
        } catch (JSONException e) {
            logger.error("Error trying to construct JSON from local folders", e);
        }

        return result.toString();
    }


    public S3MountPointFactory getNewMountPoint(RequestContext ctx) throws RepositoryException {
        AwsService awsService = (AwsService) SpringContextSingleton.getBean(AwsService.BEAN_NAME);
        RenderContext rc = getRenderContext(ctx);
        JCRSessionWrapper session = rc.getMainResource().getNode().getSession();
        Map<String, AwsAccess> accessMap = awsService.getAccessConfigs(session);
        S3MountPointFactory mountPointModel = new S3MountPointFactory();
        String editNodeId = ctx.getRequestParameters().get("edit");
        if (null != editNodeId && editNodeId.length() > 0) {
            JCRNodeWrapper nodeWrapper = session.getNodeByIdentifier(editNodeId);
            mountPointModel.populate(nodeWrapper);
        }
        ArrayList<String> accessList = new ArrayList<>();
        JSONObject bucketsMap = new JSONObject();
        for (String access : accessMap.keySet()) {
            AwsAccess awsAccess = accessMap.get(access);
            AmazonS3Client amazonS3Client = awsAccess.getClient();
            ArrayList<String> buckets = new ArrayList<>();
            try {
                for (Bucket bucket : amazonS3Client.listBuckets()) {
                    String bucketLocation = amazonS3Client.getBucketLocation(bucket.getName());
                    if (awsAccess.getRegionName().equals(bucketLocation))
                        buckets.add(bucket.getName());
                }
            } catch (AmazonServiceException e) {
                e.printStackTrace();
                continue;
            }
            if (buckets.size() > 1) {
                try {
                    bucketsMap.put(access, new JSONArray(buckets));
                    accessList.add(access);
                    if (null == mountPointModel.getDefaultBukets()) {
                        mountPointModel.setDefaultBukets(buckets);
                        mountPointModel.setAccess(access);
                    }
                } catch (JSONException ignore) {
                    ignore.printStackTrace();
                    //// TODO: 14.09.2016 Add logger
                }
            }
        }
        mountPointModel.setAccessList(accessList);
        mountPointModel.setJsonBuckets(bucketsMap.toString());
        return mountPointModel;
    }

    public Boolean save(MessageContext messageContext, RequestContext ctx) {
        stateCode = "SUCCESS";
        Locale locale = LocaleContextHolder.getLocale();
        if (null == s3MountPointFactory.getLocalPath() || s3MountPointFactory.getLocalPath().length() == 0) {
            stateCode = "ERROR";
            MessageBuilder messageBuilder = new MessageBuilder().error().defaultText(Messages.get(BUNDLE, "s3Factory.selectTarget", locale));
            messageContext.addMessage(messageBuilder.build());
            return false;
        }

        try {
            JCRSessionWrapper session = JCRTemplate.getInstance().getSessionFactory().getCurrentUserSession();
            session.getNode(s3MountPointFactory.getLocalPath());
        } catch (RepositoryException e) {
            MessageBuilder messageBuilder = new MessageBuilder().error().defaultText(Messages.get(BUNDLE, "serverSettings.s3MountPointFactory.selectTarget.error", locale));
            messageContext.addMessage(messageBuilder.build());
            return false;
        }

        AwsService awsService = (AwsService) SpringContextSingleton.getBean(AwsServiceImpl.BEAN_NAME);
        try {
            AwsAccess accessConfig = awsService.getAccessConfig(s3MountPointFactory.getAccess());
            AmazonS3Client s3 = accessConfig.getClient();
            String bLocation = s3.getBucketLocation(s3MountPointFactory.getBucket());
            if (!bLocation.equals(accessConfig.getRegionName())) {
                stateCode = "ERROR";
                MessageBuilder messageBuilder = new MessageBuilder().error().defaultText(s3MountPointFactory.getBucket() + " "
                        + Messages.get(BUNDLE, "serverSettings.s3MountPointFactory.bucket.error", locale) + " " + accessConfig.getRegionName());
                messageContext.addMessage(messageBuilder.build());
                return false;
            }
            if (null != s3MountPointFactory.getPrefix() && s3MountPointFactory.getPrefix().length() > 0) {
                ObjectMetadata objectMetadata = null;
                try {
                    objectMetadata = s3.getObjectMetadata(s3MountPointFactory.getBucket(), s3MountPointFactory.getPrefix());

                } catch (AmazonServiceException e) {
                    stateCode = "ERROR";
                    MessageBuilder messageBuilder = new MessageBuilder().error().defaultText(s3MountPointFactory.getBucket() + " "
                            + Messages.get(BUNDLE, "serverSettings.s3MountPointFactory.prefix.error", locale) + " " + s3MountPointFactory.getPrefix());
                    messageContext.addMessage(messageBuilder.build());
                    return false;
                }
            }

            // TODO: 01.09.2016 Add validate
            boolean available = super.save(s3MountPointFactory);
            if (available) {
                stateCode = "SUCCESS";
                messageKey = "serverSettings.s3MountPointFactory.save.success";
                ctx.getConversationScope().put("adminURL", getAdminURL(ctx));
                return true;
            } else {
                logger.warn("Mount point availability problem : " + s3MountPointFactory.getName() + "with the access : " + s3MountPointFactory.getAccess() + "the mount point is created but unmounted");
                stateCode = "WARNING";
                messageKey = "serverSettings.sMountPointFactory.save.unavailable";
                ctx.getConversationScope().put("adminURL", getAdminURL(ctx));
                return true;
            }
        } catch (RepositoryException e) {
            logger.error("Error saving mount point : " + s3MountPointFactory.getName(), e);
            MessageBuilder messageBuilder = new MessageBuilder().error().defaultText(Messages.get(BUNDLE, "serverSettings.s3MountPointFactory.save.error", locale));
            messageContext.addMessage(messageBuilder.build());

        }
        return false;
    }

    @Override
    public String getAdminURL(RequestContext requestContext) {
        StringBuilder builder = new StringBuilder(super.getAdminURL(requestContext));
        if (stateCode != null && messageKey != null) {
            builder.append("?stateCode=").append(stateCode).append("&messageKey=").append(messageKey).append("&bundleSource=").append(BUNDLE);
        }
        return builder.toString();
    }

    private RenderContext getRenderContext(RequestContext ctx) {
        return (RenderContext) ctx.getExternalContext().getRequestMap().get("renderContext");
    }
}
