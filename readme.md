# Jahia AWS integration module set

 Jahia AWS integration modules are the set of modules to bring the Amazon Web Services functions to Jahia-based websites. There are five modules in this set:

1. Jahia AWS - core module to connect Jahia website with AWS. This module is required for all other modules.
2. Dynamic URL Rewrite module - mandatory module to change rewrite rules in code dynamically.
3. Jahia AWS Provider - to mount AWS S3 folders as website folders.
4. Jahia AWS CloudFront - to store site media in AWS CloudFront and mount them to Jahia site.
5. Jahia AWS Elastic Transcoder - to transcode videos in mounted site folders using Elastic Transcoder.

You need an Amazone Web Services account before using this integration module. To create one, please visit https://aws.amazon.com/.

##   Installation guide
Dynamic URL Rewrite module required before the JAWSM installation. If You do not have one, please follow the instructions below.

### DYNAMIC URL REWRITE MODULE AND DYNAMIC URL REWRITE MODULE INSTALLATION.
1. Download Jahia Rewrite library*.
2. Copy it into ${jahiaWebAppRoot}\WEB-INF\lib\
3. Restart Jahia server.
4. Install Dynamic URL Rewrite module from the Jahia Public Appstore using Administration Mode (Server settings/system components/modules).

### OTHER MODULES INSTALLATION
There are four another modules to install. 
1. Jahia AWS - mandatory module.
2. Jahia AWS CloudFront - adds to Jahia CMS integration with CloudFront (Amazon Global Content Delivery Network)
3. Jahia AWS Elastic Transcoder - allows using Amazon Elastic Transcoder pre-configured pipelines to transcode site video files.
4. Jahia AWS Provider - connects Jahia with S3 Amazon scalable storage.